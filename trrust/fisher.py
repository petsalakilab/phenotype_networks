from scipy.stats import fisher_exact
import sys
def load(gene_list,threshold,modname):
    gene=gene_list #complete gene list

    descr={}
    f1=open("transdescr.csv","r") #opening descriptor file
    seq=f1.readline()
    while (seq!=""):
	    seq=seq.strip().split("\t")
	    descr[seq[0]]=seq[1] #make dictionary with descriptors in
	    seq=f1.readline()
    f1.close()

    f1=open("tfsprgene.txt","r")
    seq=f1.readline()
    fisher={}
    fisher_count={}
    while(seq!=""):
        seq=seq.strip().split("\t")
        tfs = list(set(seq[1:])) #get each tf per gene
        fisher[seq[0]]=tfs #add to dictionary
        seq=f1.readline()
    f1.close()

    f1=open("genesprTF.txt","r")
    seq=f1.readline()
    while(seq!=""):
	    seq=seq.strip().split("\t")
	    fisher_count[seq[0]]=int(seq[1]) #count of genes per tf
	    seq=f1.readline()
    f1.close()

    fisherset={}
    fisherset_count=[]
    gene_annotation={}
    for i in gene: #reorganise into big dict
	    if i in fisher:
		    for j in fisher[i]:
			    if j in gene_annotation:
				    gene_annotation[j].append(i)
			    else:
				    gene_annotation[j]=[i]

			    if j in fisherset:
				    fisherset[j] += 1
			    else:
				    fisherset[j] = 1

	    else:
		    fisherset_count.append(i)
    #whats going on here
    totalfisher=len(fisher)
    numberofgenes=len(gene)-len(list(set(fisherset_count)))

    fisher={}
    fisher_value_ic={}
    fisher_value={}
    fisher_value_no={}
    lenfisherset=len(fisherset)
    for i in fisherset:
        a=fisherset[i]
        b=numberofgenes-a
        c=fisher_count[i]-a
        d=totalfisher-a-b-c
        table=[[a,b],[c,d]]
        fisher[i]=fisher_exact(table,alternative ="greater",)[1]
        if fisher[i]<(threshold):
            if fisher_value.has_key(fisher[i]):
	            #fisher_value_ic[ic[i]].append(i)
	            fisher_value[fisher[i]].append(i)
            else:
	            #fisher_value_ic[ic[i]]=[]
	            fisher_value[fisher[i]]=[]
	            #fisher_value_ic[ic[i]].append(i)
	            fisher_value[fisher[i]].append(i)

        else:
            if fisher_value_no.has_key(fisher[i]):
	            #fisher_value_ic[ic[i]].append(i)
	            fisher_value_no[fisher[i]].append(i)
            else:
	            #fisher_value_ic[ic[i]]=[]
	            fisher_value_no[fisher[i]]=[]
	            #fisher_value_ic[ic[i]].append(i)
	            fisher_value_no[fisher[i]].append(i)
        f2=open("TRRUST_Fisher_results/{}_fisher.txt".format(modname),"w")
        for i in sorted(fisher_value):
            for j in fisher_value[i]:
                if j in descr:
                    f2.write(j+"\t"+str(i)+"\t"+str(i*lenfisherset)+"\t"+str(descr[j])+"\n")
                else:
                    f2.write(j+"\t"+str(i)+"\t"+str(i*lenfisherset)+"\t"+"No description\n")
        f2.close()
