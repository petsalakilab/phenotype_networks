RBL2_Activation	0.00321027287319	0.121990369181	No description
HCFC1_Repression	0.00641152450588	0.243637931224	No description
HDAC1_Activation	0.00797578118146	0.303079684896	No description
TRIM28_Repression	0.0127870509578	0.485907936395	No description
RBL2_Repression	0.0127870509578	0.485907936395	No description
POU2AF1_Activation	0.0159613691219	0.606532026632	No description
HDAC1_Repression	0.0200566118859	0.762151251662	No description
KAT2B_Repression	0.0285695116932	1.08564144434	No description
YY1_Activation	0.0306983352196	1.16653673835	No description
POU2F2_Activation	0.0316993723079	1.2045761477	No description
KAT2B_Activation	0.0316993723079	1.2045761477	No description
E2F4_Activation	0.05336242843	2.02777228034	No description
DNMT1_Activation	0.0594734480736	2.2599910268	No description
SIRT1_Activation	0.07159201578	2.72049659964	No description
HSF1_Activation	0.0865479377575	3.28882163478	No description
MYCN_Repression	0.0895136912062	3.40152026584	No description
DNMT1_Repression	0.0924710157983	3.51389860034	No description
