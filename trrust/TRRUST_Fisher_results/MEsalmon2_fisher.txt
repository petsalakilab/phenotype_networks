PPARG_Repression	0.000586756914995	0.0316848734097	No description
PPARG_Activation	0.000864808965379	0.0466996841305	No description
HAX1_Repression	0.00361155698234	0.195024077047	No description
ETV1_Activation	0.0072115152269	0.389421822253	No description
ZNF76_Activation	0.0072115152269	0.389421822253	No description
ETV1_Repression	0.0072115152269	0.389421822253	No description
EGR3_Activation	0.0107999073406	0.583194996392	No description
ZNF143_Activation	0.0143767658515	0.776345355981	No description
NUPR1_Activation	0.0179421232098	0.968874653331	No description
PAX3_Activation	0.0250384638761	1.35207704931	No description
PPARD_Activation	0.0250384638761	1.35207704931	No description
STAT5B_Activation	0.0250384638761	1.35207704931	No description
ATF6_Repression	0.0285695116931	1.54275363143	No description
STAT5A_Activation	0.0320891873754	1.73281611827	No description
NF1_Repression	0.0390945504982	2.1111057269	No description
STAT5A_Repression	0.0425803018268	2.29933629865	No description
NF1_Activation	0.0425803018268	2.29933629865	No description
ATF6_Activation	0.0425803018268	2.29933629865	No description
BCL6_Repression	0.0598410269472	3.23141545515	No description
XBP1_Repression	0.0598410269472	3.23141545515	No description
SOX9_Repression	0.0598410269472	3.23141545515	No description
MTA1_Activation	0.0632597868493	3.41602848986	No description
NFE2L2_Activation	0.0632597868493	3.41602848986	No description
XBP1_Activation	0.0666674917476	3.60004455437	No description
NR1I2_Activation	0.0768245889604	4.14852780386	No description
SOX9_Activation	0.0801883860038	4.33017284421	No description
SREBF1_Repression	0.0835412837471	4.51122932234	No description
HSF1_Repression	0.086883313101	4.69169890745	No description
TP53_Activation	0.087030297356	4.69963605723	No description
TP53_Repression	0.0947230981768	5.11504730155	No description
NR3C1_Activation	0.0968444988455	5.22960293765	No description
HSF1_Activation	0.0968444988455	5.22960293765	No description
