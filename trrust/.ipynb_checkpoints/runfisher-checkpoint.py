#!/usr/bin/python3
import sys
import os
import fisher

def fisher_test(filename):
    f1=open(filename,"r")

    starting_genes=[]
    seq=f1.readline()
    while(seq!=""):
        starting_genes.append(seq.strip())
        seq=f1.readline()

    modname=filename.split(".")[0]
#    if not os.path.exists("fisher/"+modname):
#        os.makedirs("fisher/"+modname)
    fisher.load(list(set(starting_genes)),0.1,modname)

infile = open(sys.argv[1], "r")
outfname = "tmp.txt"
i = 0
outfile = open(outfname, "w")

line = infile.readline()
while line != "":
    if line.startswith(">"):
        outfile.close()
        #print(outfname)
        fisher_test(outfname)
        os.remove(outfname)
        outfname= "{}.txt".format(line.strip()[1:])
        outfile = open(outfname, "w")
    outfile.write(line)
    line = infile.readline()
outfile.close()
fisher_test(outfname)
os.remove(outfname)
infile.close()

