infile = open("trrust_rawdata.human.tsv","r")
tfprgene = {}
for line in infile:
    gene = line.strip().split()[1]
    tf = line.strip().split()[0] + "_" + line.strip().split()[2]
    tf_rep = line.strip().split()[0] + "_Repression"
    tf_act = line.strip().split()[0] + "_Activation"
    if (line.strip().split()[2] == "Unknown") & (gene not in tfprgene):
        tfprgene[gene] = [tf_rep]
        tfprgene[gene].append(tf_act)
    elif (line.strip().split()[2] == "Unknown") & (gene in tfprgene): 
        tfprgene[gene].append(tf_rep)
        tfprgene[gene].append(tf_act)
    else:
        if gene not in tfprgene:
            tfprgene[gene] = [tf]
        else:
            tfprgene[gene].append(tf)
infile.close()

outfile = open("tfsprgene.txt","w")
for g in tfprgene:
    outfile.write(g + "\t" + "\t".join(tfprgene[g]) + "\n")
outfile.close()
