infile = open("trrust_rawdata.human.tsv","r")
tfcount = {}
for line in infile:
    tf = line.strip().split()[0] + "_" + line.strip().split()[2]
    if tf not in tfcount:
        tfcount[tf] = 1
    else:
        tfcount[tf] +=1
    tf_rep = line.strip().split()[0] + "_Repression"
    tf_act = line.strip().split()[0] + "_Activation"
    if line.strip().split()[2] == "Unknown":
        if tf_rep not in tfcount:
            tfcount[tf_rep] = 1
        else:
            tfcount[tf_rep] +=1
        if tf_act not in tfcount:
                tfcount[tf_act] = 1
        else:
            tfcount[tf_act] +=1
outfile = open("genesprTF+-.txt","w")
for t in tfcount:
    if str(t).split("_")[1] != "Unknown":
        outfile.write(str(t) + "\t" + str(tfcount[t]) + "\n")
outfile.close()
