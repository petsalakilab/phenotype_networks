###############################

############BLOCK 4############

library(VennDiagram)
library(dplyr)

#Use external data and train linear models using the module eigengenes as independent variables. 

shapefeat <- read.csv("../NAFLD/template_UcamSanyal.csv")

pc1<-read.delim("../NAFLD/sorted_samples according to PC1.txt", header = F, sep = " ")
all(pc1$V1[match(rownames(MEs), pc1$V1)] == shapefeat$Sample.name) # sanity check 
mod_pc1<-data.frame(MEs, pc1 = pc1$V2[match(rownames(MEs), pc1$V1)], shapefeat)
variables<-colnames(mod_pc1)[42:46]
#we generate a linear model for each of NAS, Fibrosis, Fat, Ballooning and inflammation 
venn<-list()

#for each variable
for (variable in variables) {
  # run n regressions
  my_lms <- lapply(colnames(MEs), function(x) lm(mod_pc1[variable][,1] ~ mod_pc1[x][,1] + mod_pc1$Age))
  # extract just summaries
  summaries <- lapply(my_lms, summary)
  residuals <- lapply(my_lms, residuals)
  names(residuals)<-colnames(MEs)
  fitted.values <- lapply(my_lms, fitted.values)
  names(fitted.values)<-colnames(MEs)
  # ...coefficents with p values:
  coef_pvalues<-lapply(summaries, function(x) x$coefficients[, c(1,4)])
  names(coef_pvalues)<-colnames(MEs)
  coef<-lapply(colnames(MEs), function(x) data.frame(coef_pvalues[x][[1]], module = x, statistic = rownames(coef_pvalues[x][[1]])))
  bound_coef<-bind_rows(coef)
  #multiple testing correction <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<!!!!!!!!!!!!!!!!!!!
  bound_coef$adj.p<-p.adjust(bound_coef$Pr...t.., method = p.adjust.methods[1])
  rownames(bound_coef)<-NULL
  venn[variable]<-list(unique(bound_coef[bound_coef$adj.p<0.05 & bound_coef$statistic == "mod_pc1[x][, 1]",]$module))
}



