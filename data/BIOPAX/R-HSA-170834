<?xml version="1.0" encoding="UTF-8"?>
<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:owl="http://www.w3.org/2002/07/owl#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" xmlns:bp="http://www.biopax.org/release/biopax-level3.owl#" xml:base="http://pathwaycommons.org/pc12/">
  <owl:Ontology rdf:about="">
    <owl:imports rdf:resource="http://www.biopax.org/release/biopax-level3.owl#"/>
  </owl:Ontology>
  <bp:RelationshipTypeVocabulary rdf:about="http://identifiers.org/psimi/MI:0359">
    <bp:xref rdf:resource="#UnificationXref_molecular_interactions_ontology_MI_0359"/>
    <bp:term rdf:datatype="http://www.w3.org/2001/XMLSchema#string">process</bp:term>
    <bp:term rdf:datatype="http://www.w3.org/2001/XMLSchema#string">gene ontology term for cellular process</bp:term>
    <bp:term rdf:datatype="http://www.w3.org/2001/XMLSchema#string">biological process</bp:term>
  </bp:RelationshipTypeVocabulary>
  <bp:UnificationXref rdf:ID="UnificationXref_reactome_R-HSA-170834">
    <bp:idVersion rdf:datatype="http://www.w3.org/2001/XMLSchema#string">1</bp:idVersion>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Reactome stable identifier. Use this URL to connect to the web page of this instance in Reactome: http://www.reactome.org/cgi-bin/eventbrowser_st_id?ST_ID=R-HSA-170834.1</bp:comment>
    <bp:id rdf:datatype="http://www.w3.org/2001/XMLSchema#string">R-HSA-170834</bp:id>
    <bp:db rdf:datatype="http://www.w3.org/2001/XMLSchema#string">reactome</bp:db>
  </bp:UnificationXref>
  <bp:PublicationXref rdf:about="http://identifiers.org/pubmed/19648010">
    <bp:year rdf:datatype="http://www.w3.org/2001/XMLSchema#int">2009</bp:year>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Kang, JS</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Liu, C</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Derynck, R</bp:author>
    <bp:id rdf:datatype="http://www.w3.org/2001/XMLSchema#string">19648010</bp:id>
    <bp:source rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Trends Cell Biol 19:385-94</bp:source>
    <bp:title rdf:datatype="http://www.w3.org/2001/XMLSchema#string">New regulatory mechanisms of TGF-beta receptor function</bp:title>
    <bp:db rdf:datatype="http://www.w3.org/2001/XMLSchema#string">pubmed</bp:db>
  </bp:PublicationXref>
  <bp:Pathway rdf:about="http://identifiers.org/reactome/R-HSA-2173793">
    <bp:organism rdf:resource="http://identifiers.org/taxonomy/9606"/>
    <bp:displayName rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Transcriptional activity of SMAD2/SMAD3:SMAD4 heterotrimer</bp:displayName>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Authored: Orlic-Milacic, M, 2012-04-04</bp:comment>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">In the nucleus, SMAD2/3:SMAD4 heterotrimer complex acts as a transcriptional regulator. The activity of SMAD2/3 complex is regulated both positively and negatively by association with other transcription factors (Chen et al. 2002, Varelas et al. 2008, Stroschein et al. 1999, Wotton et al. 1999). In addition, the activity of SMAD2/3:SMAD4 complex can be inhibited by nuclear protein phosphatases and ubiquitin ligases (Lin et al. 2006, Dupont et al. 2009).</bp:comment>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">REPLACED http://identifiers.org/reactome/R-HSA-2173793</bp:comment>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Reviewed: Huang, Tao, 2012-05-14</bp:comment>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Reviewed: Chen, Ye-Guang, 2012-11-14</bp:comment>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Edited: Jassal, B, 2012-04-10</bp:comment>
    <bp:dataSource rdf:resource="#Provenance_2fd94e28e36931796fb9119fad6d4003"/>
  </bp:Pathway>
  <bp:Pathway rdf:about="http://identifiers.org/reactome/R-HSA-2173791">
    <bp:xref rdf:resource="#RelationshipXref_c2af8f85-d494-421e-aa4c-1aeacc2cd4b7http___www_reactome_org_biopax_69_48887_RelationshipXref2229"/>
    <bp:organism rdf:resource="http://identifiers.org/taxonomy/9606"/>
    <bp:displayName rdf:datatype="http://www.w3.org/2001/XMLSchema#string">TGF-beta receptor signaling in EMT (epithelial to mesenchymal transition)</bp:displayName>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Authored: Orlic-Milacic, M, 2012-04-04</bp:comment>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Reviewed: Chen, Ye-Guang, 2012-11-14</bp:comment>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">REPLACED http://identifiers.org/reactome/R-HSA-2173791</bp:comment>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Reviewed: Huang, Tao, 2012-05-14</bp:comment>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Edited: Jassal, B, 2012-04-10</bp:comment>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">In normal cells and in the early stages of cancer development, signaling by TGF-beta plays a tumor suppressive role, as SMAD2/3:SMAD4-mediated transcription inhibits cell division by downregulating MYC oncogene transcription and stimulating transcription of CDKN2B tumor suppressor gene. In advanced cancers however, TGF-beta signaling promotes metastasis by stimulating epithelial to mesenchymal transition (EMT). &lt;br&gt;TGFBR1 is recruited to tight junctions by binding PARD6A, a component of tight junctions. After TGF-beta stimulation, activated TGFBR2 binds TGFBR1 at tight junctions, and phosphorylates both TGFBR1 and PARD6A. Phosphorylated PARD6A recruits SMURF1 to tight junctions. SMURF1 is able to ubiquitinate RHOA, a component of tight junctions needed for tight junction maintenance, leading to disassembly of tight junctions, an important step in EMT (Wang et al. 2003, Ozdamar et al. 2005).</bp:comment>
    <bp:dataSource rdf:resource="#Provenance_2fd94e28e36931796fb9119fad6d4003"/>
  </bp:Pathway>
  <bp:BioSource rdf:about="http://identifiers.org/taxonomy/9606">
    <bp:xref rdf:resource="#UnificationXref_taxonomy_9606"/>
    <bp:displayName rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Homo sapiens</bp:displayName>
    <bp:name rdf:datatype="http://www.w3.org/2001/XMLSchema#string">BiologicalState8</bp:name>
    <bp:name rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Homo sapiens (human)</bp:name>
    <bp:name rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Human</bp:name>
    <bp:name rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Homo sapiens (Human)</bp:name>
    <bp:name rdf:datatype="http://www.w3.org/2001/XMLSchema#string">human</bp:name>
  </bp:BioSource>
  <bp:UnificationXref rdf:ID="UnificationXref_taxonomy_9606">
    <bp:id rdf:datatype="http://www.w3.org/2001/XMLSchema#string">9606</bp:id>
    <bp:db rdf:datatype="http://www.w3.org/2001/XMLSchema#string">taxonomy</bp:db>
  </bp:UnificationXref>
  <bp:PathwayStep rdf:ID="PathwayStep_cf3fd930540541ae67a7fd332a83668c">
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">REPLACED http://pathwaycommons.org/pc12/PathwayStep_cf3fd930540541ae67a7fd332a83668c</bp:comment>
    <bp:stepProcess rdf:resource="http://identifiers.org/reactome/R-HSA-2173791"/>
  </bp:PathwayStep>
  <bp:Pathway rdf:about="http://identifiers.org/reactome/R-HSA-170834">
    <bp:pathwayOrder rdf:resource="#PathwayStep_cf3fd930540541ae67a7fd332a83668c"/>
    <bp:pathwayOrder rdf:resource="#PathwayStep_0b0abe4940e2492bdefd30377212924e"/>
    <bp:pathwayOrder rdf:resource="#PathwayStep_2113d5328deeab5d2d2d976c9ef917ff"/>
    <bp:pathwayComponent rdf:resource="http://identifiers.org/reactome/R-HSA-2173793"/>
    <bp:pathwayComponent rdf:resource="http://identifiers.org/reactome/R-HSA-2173791"/>
    <bp:pathwayComponent rdf:resource="http://identifiers.org/reactome/R-HSA-2173789"/>
    <bp:xref rdf:resource="#UnificationXref_reactome_R-HSA-170834"/>
    <bp:xref rdf:resource="http://identifiers.org/pubmed/19648010"/>
    <bp:xref rdf:resource="#RelationshipXref_c2af8f85-d494-421e-aa4c-1aeacc2cd4b7http___www_reactome_org_biopax_69_48887_RelationshipXref2229"/>
    <bp:organism rdf:resource="http://identifiers.org/taxonomy/9606"/>
    <bp:displayName rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Signaling by TGF-beta Receptor Complex</bp:displayName>
    <bp:name rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Transforming Growth Factor (TGF) beta signaling</bp:name>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Reviewed: Heldin, CH, 2006-04-18 14:26:12</bp:comment>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">REPLACED http://identifiers.org/reactome/R-HSA-170834</bp:comment>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Reviewed: Chen, Ye-Guang, 2012-11-14</bp:comment>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Authored: Heldin, CH, Moustakas, A, Huminiecki, L, Jassal, B, 2006-02-02</bp:comment>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Reviewed: Huang, Tao, 2012-05-14</bp:comment>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">The TGF-beta/BMP pathway incorporates several signaling pathways that share most, but not all, components of a central signal transduction engine. The general signaling scheme is rather simple: upon binding of a ligand, an activated plasma membrane receptor complex is formed, which passes on the signal towards the nucleus through a phosphorylated receptor SMAD (R-SMAD). In the nucleus, the activated R-SMAD promotes transcription in complex with a closely related helper molecule termed Co-SMAD (SMAD4). However, this simple linear pathway expands into a network when various regulatory components and mechanisms are taken into account. The signaling pathway includes a great variety of different TGF-beta/BMP superfamily ligands and receptors, several types of the R-SMADs, and functionally critical negative feedback loops. The R-SMAD:Co-SMAD complex can interact with a great number of transcriptional co-activators/co-repressors to regulate positively or negatively effector genes, so that the interpretation of a signal depends on the cell-type and cross talk with other signaling pathways such as Notch, MAPK and Wnt. The pathway plays a number of different biological roles in the control of embryonic and adult cell proliferation and differentiation, and it is implicated in a great number of human diseases.&lt;br&gt;TGF beta (TGFB1) is secreted as a homodimer, and as such it binds to TGF beta receptor II (TGFBR2), inducing its dimerization. Binding of TGF beta enables TGFBR2 to form a stable hetero-tetrameric complex with TGF beta receptor I homodimer (TGFBR1). TGFBR2 acts as a serine/threonine kinase and phosphorylates serine and threonine residues within the short GS domain (glycine-serine rich domain) of TGFBR1.&lt;br&gt;The phosphorylated heterotetrameric TGF beta receptor complex (TGFBR) internalizes into clathrin coated endocytic vesicles where it associates with the endosomal membrane protein SARA. SARA facilitates the recruitment of cytosolic SMAD2 and SMAD3, which act as R-SMADs for TGF beta receptor complex. TGFBR1 phosphorylates recruited SMAD2 and SMAD3, inducing a conformational change that promotes formation of R-SMAD trimers and dissociation of R-SMADs from the TGF beta receptor complex. &lt;br&gt;In the cytosol, phosphorylated SMAD2 and SMAD3 associate with SMAD4 (known as Co-SMAD), forming a heterotrimer which is more stable than the R-SMAD homotrimers. R-SMAD:Co-SMAD heterotrimer translocates to the nucleus where it directly binds DNA and, in cooperation with other transcription factors, regulates expression of genes involved in cell differentiation, in a context-dependent manner. &lt;br&gt;The intracellular level of SMAD2 and SMAD3 is regulated by SMURF ubiquitin ligases, which target R-SMADs for degradation. In addition, nuclear R-SMAD:Co-SMAD heterotrimer stimulates transcription of inhibitory SMADs (I-SMADs), forming a negative feedback loop. I-SMADs bind the phosphorylated TGF beta receptor complexes on caveolin coated vesicles, derived from the lipid rafts, and recruit SMURF ubiquitin ligases to TGF beta receptors, leading to ubiquitination and degradation of TGFBR1. Nuclear R-SMAD:Co-SMAD heterotrimers are targets of nuclear ubiquitin ligases which ubiquitinate SMAD2/3 and SMAD4, causing heterotrimer dissociation, translocation of ubiquitinated SMADs to the cytosol and their proteasome-mediated degradation. For a recent review of TGF-beta receptor signaling, please refer to Kang et al. 2009.</bp:comment>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Edited: Jassal, B, 2012-04-10</bp:comment>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Edited: Jassal, B, 2006-04-18 13:31:36</bp:comment>
    <bp:dataSource rdf:resource="#Provenance_2fd94e28e36931796fb9119fad6d4003"/>
  </bp:Pathway>
  <bp:Provenance rdf:ID="Provenance_2fd94e28e36931796fb9119fad6d4003">
    <bp:standardName rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Reactome</bp:standardName>
    <bp:displayName rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Reactome</bp:displayName>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">REPLACED http://pathwaycommons.org/pc12/Provenance_2fd94e28e36931796fb9119fad6d4003</bp:comment>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Source http://www.reactome.org/download/current/biopax.zip type: BIOPAX, Reactome v69 (only 'Homo_sapiens.owl') 28-May-2019</bp:comment>
  </bp:Provenance>
  <bp:Pathway rdf:about="http://identifiers.org/reactome/R-HSA-2173789">
    <bp:xref rdf:resource="#RelationshipXref_c2af8f85-d494-421e-aa4c-1aeacc2cd4b7http___www_reactome_org_biopax_69_48887_RelationshipXref2229"/>
    <bp:organism rdf:resource="http://identifiers.org/taxonomy/9606"/>
    <bp:displayName rdf:datatype="http://www.w3.org/2001/XMLSchema#string">TGF-beta receptor signaling activates SMADs</bp:displayName>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Authored: Orlic-Milacic, M, 2012-04-04</bp:comment>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Reviewed: Chen, Ye-Guang, 2012-11-14</bp:comment>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">REPLACED http://identifiers.org/reactome/R-HSA-2173789</bp:comment>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Reviewed: Huang, Tao, 2012-05-14</bp:comment>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Edited: Jassal, B, 2012-04-10</bp:comment>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Binding of transforming growth factor beta 1 (TGF beta 1, i.e. TGFB1) to TGF beta receptor type 2 (TGFBR2) activates TGF beta receptor signaling cascade. TGFB1 is posttranslationally processed by furin (Dubois et al. 1995) to form a homodimer and secreted to the extracellular space as part of the large latent complex (LLC). After the LLC disassembles in the extracellular space, dimeric TGFB1 becomes capable of binding to TGFBR2 (Annes et al. 2003, Keski Oja et al. 2004). Formation of TGFB1:TGFBR2 complex creates a binding pocket for TGF-beta receptor type-1 (TGFBR1) and TGFBR1 is recruited to the complex by binding to both TGFB1 and TGFBR2. This results in an active heterotetrameric TGF-beta receptor complex that consists of TGFB1 homodimer bound to two heterodimers of TGFBR1 and TGFBR2 (Wrana et al. 1992, Moustakas et al. 1993, Franzen et al. 1993). TGF-beta signaling can also occur through a single heterodimer of TGFBR1 and TGFBR2, although with decreased efficiency (Huang et al. 2011). TGFBR1 and TGFBR2 interact through their extracellular domains, which brings their cytoplasmic domains together. Ligand binding to extracellular receptor domains is cooperative, but no conformational change is seen from crystal structures of either TGFB1- or TGFB3-bound heterotetrameric receptor complexes (Groppe et al. 2008, Radaev et al. 2010).&lt;br&gt;&lt;br&gt;Activation of TGFBR1 by TGFBR2 in the absence of ligand is prevented by FKBP1A (FKBP12), a peptidyl-prolyl cis-trans isomerase. FKBP1A forms a complex with inactive TGFBR1 and dissociates from it only after TGFBR1 is recruited by TGFB1-bound TGFBR2 (Chen et al. 1997). &lt;br&gt;&lt;br&gt;Both TGFBR1 and TGFBR2 are receptor serine/threonine kinases. Formation of the hetero-tetrameric TGF-beta receptor complex (TGFBR) in response to TGFB1 binding induces receptor rotation, so that TGFBR2 and TGFBR1 cytoplasmic kinase domains face each other in a catalytically favourable configuration. TGFBR2 trans-phosphorylates serine residues at the conserved Gly-Ser-rich juxtapositioned domain (GS domain) of TGFBR1 (Wrana et al. 1994, Souchelnytskyi et al. 1996), activating TGFBR1.&lt;br&gt;In addition to phosphorylation, TGFBR1 may also be sumoylated in response to TGF-beta stimulation. Sumoylation enhances TGFBR1 kinase activity (Kang et al. 2008). &lt;br&gt;&lt;br&gt;The activated TGFBR complex is internalized by clathrin-mediated endocytosis into early endosomes. With the assistance of SARA, an early endosome membrane protein, phosphorylated TGFBR1 within TGFBR complex recruits SMAD2 and/or SMAD3 , i.e. R-SMADs (Tsukazaki et al. 1998). TGFBR1 phosphorylates recruited SMAD2/3 on two C-terminal serine residues (Souchelnytskyi et al. 2001). The phosphorylation changes the conformation of SMAD2/3 MH2 domain, promoting dissociation of SMAD2/3 from SARA and TGFBR1 (Souchelnytskyi et al. 1997, Macias-Silva et al. 1996, Nakao et al. 1997) and formation of SMAD2/3 trimers (Chacko et al. 2004). The phosphorylated C-terminal tail of SMAD2/3 has high affinity for SMAD4 (Co-SMAD), inducing formation of SMAD2/3:SMAD4 heterotrimers, composed of two phosphorylated R-SMADs (SMAD2 and/or SMAD3) and SMAD4 (Co-SMAD). SMAD2/3:SMAD4 heterotrimers are energetically favored over R-SMAD trimers (Nakao et al. 1997, Qin et al. 2001, Kawabata et al. 1998, Chacko et al. 2004). &lt;br&gt;SMAD2/3:SMAD4 heterotrimers translocate to the nucleus where they act as transcriptional regulators.</bp:comment>
    <bp:dataSource rdf:resource="#Provenance_2fd94e28e36931796fb9119fad6d4003"/>
  </bp:Pathway>
  <bp:UnificationXref rdf:ID="UnificationXref_molecular_interactions_ontology_MI_0359">
    <bp:id rdf:datatype="http://www.w3.org/2001/XMLSchema#string">MI:0359</bp:id>
    <bp:db rdf:datatype="http://www.w3.org/2001/XMLSchema#string">molecular interactions ontology</bp:db>
  </bp:UnificationXref>
  <bp:PathwayStep rdf:ID="PathwayStep_0b0abe4940e2492bdefd30377212924e">
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">REPLACED http://pathwaycommons.org/pc12/PathwayStep_0b0abe4940e2492bdefd30377212924e</bp:comment>
    <bp:stepProcess rdf:resource="http://identifiers.org/reactome/R-HSA-2173789"/>
  </bp:PathwayStep>
  <bp:PathwayStep rdf:ID="PathwayStep_2113d5328deeab5d2d2d976c9ef917ff">
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">REPLACED http://pathwaycommons.org/pc12/PathwayStep_2113d5328deeab5d2d2d976c9ef917ff</bp:comment>
    <bp:stepProcess rdf:resource="http://identifiers.org/reactome/R-HSA-2173793"/>
  </bp:PathwayStep>
  <bp:RelationshipXref rdf:ID="RelationshipXref_c2af8f85-d494-421e-aa4c-1aeacc2cd4b7http___www_reactome_org_biopax_69_48887_RelationshipXref2229">
    <bp:relationshipType rdf:resource="http://identifiers.org/psimi/MI:0359"/>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">REPLACED http://pathwaycommons.org/pc12/RelationshipXref_c2af8f85-d494-421e-aa4c-1aeacc2cd4b7http___www_reactome_org_biopax_69_48887_RelationshipXref2229</bp:comment>
    <bp:id rdf:datatype="http://www.w3.org/2001/XMLSchema#string">GO:0007179</bp:id>
    <bp:db rdf:datatype="http://www.w3.org/2001/XMLSchema#string">gene ontology</bp:db>
  </bp:RelationshipXref>
</rdf:RDF>
