<?xml version="1.0" encoding="UTF-8"?>
<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:owl="http://www.w3.org/2002/07/owl#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" xmlns:bp="http://www.biopax.org/release/biopax-level3.owl#" xml:base="http://pathwaycommons.org/pc12/">
  <owl:Ontology rdf:about="">
    <owl:imports rdf:resource="http://www.biopax.org/release/biopax-level3.owl#"/>
  </owl:Ontology>
  <bp:RelationshipTypeVocabulary rdf:about="http://identifiers.org/psimi/MI:0359">
    <bp:xref rdf:resource="#UnificationXref_molecular_interactions_ontology_MI_0359"/>
    <bp:term rdf:datatype="http://www.w3.org/2001/XMLSchema#string">process</bp:term>
    <bp:term rdf:datatype="http://www.w3.org/2001/XMLSchema#string">gene ontology term for cellular process</bp:term>
    <bp:term rdf:datatype="http://www.w3.org/2001/XMLSchema#string">biological process</bp:term>
  </bp:RelationshipTypeVocabulary>
  <bp:PublicationXref rdf:about="http://identifiers.org/pubmed/19135894">
    <bp:year rdf:datatype="http://www.w3.org/2001/XMLSchema#int">2009</bp:year>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Stinchfield, MJ</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Inui, M</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Martello, G</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Montagner, M</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Cordenonsi, M</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Soligo, S</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Morsut, L</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Modena, N</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Adorno, M</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Piccolo, S</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Zacchigna, L</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Newfeld, SJ</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Dupont, S</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Argenton, F</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Moro, S</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Mamidi, A</bp:author>
    <bp:id rdf:datatype="http://www.w3.org/2001/XMLSchema#string">19135894</bp:id>
    <bp:source rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Cell 136:123-35</bp:source>
    <bp:title rdf:datatype="http://www.w3.org/2001/XMLSchema#string">FAM/USP9x, a deubiquitinating enzyme essential for TGFbeta signaling, controls Smad4 monoubiquitination</bp:title>
    <bp:db rdf:datatype="http://www.w3.org/2001/XMLSchema#string">pubmed</bp:db>
  </bp:PublicationXref>
  <bp:Pathway rdf:about="http://identifiers.org/reactome/R-HSA-2173796">
    <bp:xref rdf:resource="http://identifiers.org/pubmed/18568018"/>
    <bp:xref rdf:resource="http://identifiers.org/pubmed/12150994"/>
    <bp:organism rdf:resource="http://identifiers.org/taxonomy/9606"/>
    <bp:displayName rdf:datatype="http://www.w3.org/2001/XMLSchema#string">SMAD2/SMAD3:SMAD4 heterotrimer regulates transcription</bp:displayName>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Authored: Orlic-Milacic, M, 2012-04-04</bp:comment>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">REPLACED http://identifiers.org/reactome/R-HSA-2173796</bp:comment>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">After phosphorylated SMAD2 and/or SMAD3 form a heterotrimer with SMAD4, SMAD2/3:SMAD4 complex translocates to the nucleus (Xu et al. 2000, Kurisaki et al. 2001, Xiao et al. 2003). In the nucleus, linker regions of SMAD2 and SMAD3 within SMAD2/3:SMAD4 complex can be phosphorylated by CDK8 associated with cyclin C (CDK8:CCNC) or CDK9 associated with cyclin T (CDK9:CCNT). CDK8/CDK9-mediated phosphorylation of SMAD2/3 enhances transcriptional activity of SMAD2/3:SMAD4 complex, but also primes it for ubiquitination and consequent degradation (Alarcon et al. 2009). &lt;br&gt;&lt;br&gt;The transfer of SMAD2/3:SMAD4 complex to the nucleus can be assisted by other proteins, such as WWTR1. In human embryonic cells, WWTR1 (TAZ) binds SMAD2/3:SMAD4 heterotrimer and mediates TGF-beta-dependent nuclear accumulation of SMAD2/3:SMAD4. The complex of WWTR1 and SMAD2/3:SMAD4 binds promoters of SMAD7 and SERPINE1 (PAI-1 i.e. plasminogen activator inhibitor 1) genes and stimulates their transcription (Varelas et al. 2008). Stimulation of SMAD7 transcription by SMAD2/3:SMAD4 represents a negative feedback loop in TGF-beta receptor signaling. SMAD7 can be downregulated by RNF111 ubiquitin ligase (Arkadia), which binds and ubiquitinates SMAD7, targeting it for degradation (Koinuma et al. 2003). &lt;br&gt;&lt;br&gt;SMAD2/3:SMAD4 heterotrimer also binds the complex of RBL1 (p107), E2F4/5 and TFDP1/2 (DP1/2). The resulting complex binds MYC promoter and inhibits MYC transcription. Inhibition of MYC transcription contributes to anti-proliferative effect of TGF-beta (Chen et al. 2002). SMAD2/3:SMAD4 heterotrimer also associates with transcription factor SP1. SMAD2/3:SMAD4:SP1 complex stimulates transcription of a CDK inhibitor CDKN2B (p15-INK4B), also contributing to the anti-proliferative effect of TGF-beta (Feng et al. 2000). &lt;br&gt;&lt;br&gt;MEN1 (menin), a transcription factor tumor suppressor mutated in a familial cancer syndrome multiple endocrine neoplasia type 1, forms a complex with SMAD2/3:SMAD4 heterotrimer, but transcriptional targets of SMAD2/3:SMAD4:MEN1 have not been elucidated (Kaji et al. 2001, Sowa et al. 2004, Canaff et al. 2012). &lt;br&gt;&lt;br&gt;JUNB is also an established transcriptional target of SMAD2/3:SMAD4 complex (Wong et al. 1999).</bp:comment>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Edited: Jassal, B, 2012-04-10</bp:comment>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Reviewed: Huang, Tao, 2012-05-14</bp:comment>
    <bp:dataSource rdf:resource="#Provenance_2fd94e28e36931796fb9119fad6d4003"/>
  </bp:Pathway>
  <bp:Pathway rdf:about="http://identifiers.org/reactome/R-HSA-2173795">
    <bp:xref rdf:resource="http://identifiers.org/pubmed/19135894"/>
    <bp:xref rdf:resource="http://identifiers.org/pubmed/16751101"/>
    <bp:xref rdf:resource="http://identifiers.org/pubmed/10199400"/>
    <bp:xref rdf:resource="http://identifiers.org/pubmed/10531062"/>
    <bp:organism rdf:resource="http://identifiers.org/taxonomy/9606"/>
    <bp:displayName rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Downregulation of SMAD2/3:SMAD4 transcriptional activity</bp:displayName>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Authored: Orlic-Milacic, M, 2012-04-04</bp:comment>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">REPLACED http://identifiers.org/reactome/R-HSA-2173795</bp:comment>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Edited: Jassal, B, 2012-04-10</bp:comment>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Transcriptional activity of SMAD2/3:SMAD4 heterotrimer can be inhibited by formation of a complex with SKI or SKIL (SNO), where SKI or SKIL recruit NCOR and possibly other transcriptional repressors to SMAD-binding promoter elements (Sun et al. 1999, Luo et al. 1999, Strochein et al. 1999). Higher levels of phosphorylated SMAD2 and SMAD3, however, may target SKI and SKIL for degradation (Strochein et al. 1999, Sun et al. 1999 PNAS, Bonni et al. 2001) through recruitment of SMURF2 (Bonni et al. 2001) or RNF111 i.e. Arkadia (Levy et al. 2007) ubiquitin ligases to SKI/SKIL by SMAD2/3. Therefore,the ratio of SMAD2/3 and SKI/SKIL determines the outcome: inhibition of SMAD2/3:SMAD4-mediated transcription or degradation of SKI/SKIL. SKI and SKIL are overexpressed in various cancer types and their oncogenic effect is connected with their ability to inhibit signaling by TGF-beta receptor complex. &lt;br&gt;SMAD4 can be monoubiquitinated by a nuclear ubiquitin ligase TRIM33 (Ecto, Ectodermin, Tif1-gamma). Monoubiquitination of SMAD4 disrupts SMAD2/3:SMAD4 heterotrimers and leads to SMAD4 translocation to the cytosol. In the cytosol, SMAD4 can be deubiquitinated by USP9X (FAM), reversing TRIM33-mediated negative regulation (Dupont et al. 2009).&lt;br&gt;Phosphorylation of the linker region of SMAD2 and SMAD3 by CDK8 or CDK9 primes SMAD2/3:SMAD4 complex for ubiquitination by NEDD4L and SMURF ubiquitin ligases. NEDD4L ubiquitinates SMAD2/3 and targets SMAD2/3:SMAD4 heterotrimer for degradation (Gao et al. 2009). SMURF2 monoubiquitinates SMAD2/3, leading to disruption of SMAD2/3:SMAD4 complexes (Tang et al. 2011). &lt;br&gt;Transcriptional repressors TGIF1 and TGIF2 bind SMAD2/3:SMAD4 complexes and inhibit SMAD-mediated transcription by recruitment of histone deacetylase HDAC1 to SMAD-binding promoter elements (Wotton et al. 1999, Melhuish et al. 2001).&lt;br&gt;PARP1 can attach poly ADP-ribosyl chains to SMAD3 and SMAD4 within SMAD2/3:SMAD4 heterotrimers. PARylated SMAD2/3:SMAD4 complexes are unable to bind SMAD-binding DNA elements (SBEs) (Lonn et al. 2010). &lt;br&gt;Phosphorylated SMAD2 and SMAD3 can be dephosphorylated by PPM1A protein phosphatase, leading to dissociation of SMAD2/3 complexes and translocation of unphosphorylated SMAD2/3 to the cytosol (Lin et al. 2006).</bp:comment>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Reviewed: Huang, Tao, 2012-05-14</bp:comment>
    <bp:dataSource rdf:resource="#Provenance_2fd94e28e36931796fb9119fad6d4003"/>
  </bp:Pathway>
  <bp:Pathway rdf:about="http://identifiers.org/reactome/R-HSA-2173793">
    <bp:pathwayOrder rdf:resource="#PathwayStep_97a2dd0601ae9e7753a4ec957cdd0486"/>
    <bp:pathwayOrder rdf:resource="#PathwayStep_8be59743477e0ef5ef48278c592c3e8c"/>
    <bp:pathwayComponent rdf:resource="http://identifiers.org/reactome/R-HSA-2173795"/>
    <bp:pathwayComponent rdf:resource="http://identifiers.org/reactome/R-HSA-2173796"/>
    <bp:xref rdf:resource="http://identifiers.org/pubmed/18568018"/>
    <bp:xref rdf:resource="http://identifiers.org/pubmed/12150994"/>
    <bp:xref rdf:resource="http://identifiers.org/pubmed/16751101"/>
    <bp:xref rdf:resource="http://identifiers.org/pubmed/10531062"/>
    <bp:xref rdf:resource="http://identifiers.org/pubmed/19135894"/>
    <bp:xref rdf:resource="#RelationshipXref_a0fec1a4-7ef6-4374-aaca-4124cbcba505http___www_reactome_org_biopax_69_48887_RelationshipXref2240"/>
    <bp:xref rdf:resource="http://identifiers.org/pubmed/10199400"/>
    <bp:xref rdf:resource="#UnificationXref_reactome_R-HSA-2173793"/>
    <bp:organism rdf:resource="http://identifiers.org/taxonomy/9606"/>
    <bp:displayName rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Transcriptional activity of SMAD2/SMAD3:SMAD4 heterotrimer</bp:displayName>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Authored: Orlic-Milacic, M, 2012-04-04</bp:comment>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">In the nucleus, SMAD2/3:SMAD4 heterotrimer complex acts as a transcriptional regulator. The activity of SMAD2/3 complex is regulated both positively and negatively by association with other transcription factors (Chen et al. 2002, Varelas et al. 2008, Stroschein et al. 1999, Wotton et al. 1999). In addition, the activity of SMAD2/3:SMAD4 complex can be inhibited by nuclear protein phosphatases and ubiquitin ligases (Lin et al. 2006, Dupont et al. 2009).</bp:comment>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">REPLACED http://identifiers.org/reactome/R-HSA-2173793</bp:comment>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Reviewed: Huang, Tao, 2012-05-14</bp:comment>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Reviewed: Chen, Ye-Guang, 2012-11-14</bp:comment>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Edited: Jassal, B, 2012-04-10</bp:comment>
    <bp:dataSource rdf:resource="#Provenance_2fd94e28e36931796fb9119fad6d4003"/>
  </bp:Pathway>
  <bp:PublicationXref rdf:about="http://identifiers.org/pubmed/10199400">
    <bp:year rdf:datatype="http://www.w3.org/2001/XMLSchema#int">1999</bp:year>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Lo, RS</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Lee, S</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Wotton, D</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Massague, J</bp:author>
    <bp:id rdf:datatype="http://www.w3.org/2001/XMLSchema#string">10199400</bp:id>
    <bp:source rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Cell 97:29-39</bp:source>
    <bp:title rdf:datatype="http://www.w3.org/2001/XMLSchema#string">A Smad transcriptional corepressor</bp:title>
    <bp:db rdf:datatype="http://www.w3.org/2001/XMLSchema#string">pubmed</bp:db>
  </bp:PublicationXref>
  <bp:PublicationXref rdf:about="http://identifiers.org/pubmed/16751101">
    <bp:year rdf:datatype="http://www.w3.org/2001/XMLSchema#int">2006</bp:year>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Chen, YG</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Hu, M</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Wang, J</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Long, J</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Shi, Y</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Davis, CM</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Meng, A</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Liang, YY</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Wrighton, KH</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Duan, X</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Feng, XH</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Lin, X</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Brunicardi, FC</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Su, Y</bp:author>
    <bp:id rdf:datatype="http://www.w3.org/2001/XMLSchema#string">16751101</bp:id>
    <bp:source rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Cell 125:915-28</bp:source>
    <bp:title rdf:datatype="http://www.w3.org/2001/XMLSchema#string">PPM1A functions as a Smad phosphatase to terminate TGFbeta signaling</bp:title>
    <bp:db rdf:datatype="http://www.w3.org/2001/XMLSchema#string">pubmed</bp:db>
  </bp:PublicationXref>
  <bp:PathwayStep rdf:ID="PathwayStep_97a2dd0601ae9e7753a4ec957cdd0486">
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">REPLACED http://pathwaycommons.org/pc12/PathwayStep_97a2dd0601ae9e7753a4ec957cdd0486</bp:comment>
    <bp:stepProcess rdf:resource="http://identifiers.org/reactome/R-HSA-2173796"/>
  </bp:PathwayStep>
  <bp:RelationshipXref rdf:ID="RelationshipXref_a0fec1a4-7ef6-4374-aaca-4124cbcba505http___www_reactome_org_biopax_69_48887_RelationshipXref2240">
    <bp:relationshipType rdf:resource="http://identifiers.org/psimi/MI:0359"/>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">REPLACED http://pathwaycommons.org/pc12/RelationshipXref_a0fec1a4-7ef6-4374-aaca-4124cbcba505http___www_reactome_org_biopax_69_48887_RelationshipXref2240</bp:comment>
    <bp:id rdf:datatype="http://www.w3.org/2001/XMLSchema#string">GO:0006351</bp:id>
    <bp:db rdf:datatype="http://www.w3.org/2001/XMLSchema#string">gene ontology</bp:db>
  </bp:RelationshipXref>
  <bp:PublicationXref rdf:about="http://identifiers.org/pubmed/12150994">
    <bp:year rdf:datatype="http://www.w3.org/2001/XMLSchema#int">2002</bp:year>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Siegel, PM</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Chen, CR</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Kang, Y</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Massague, J</bp:author>
    <bp:id rdf:datatype="http://www.w3.org/2001/XMLSchema#string">12150994</bp:id>
    <bp:source rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Cell 110:19-32</bp:source>
    <bp:title rdf:datatype="http://www.w3.org/2001/XMLSchema#string">E2F4/5 and p107 as Smad cofactors linking the TGFbeta receptor to c-myc repression</bp:title>
    <bp:db rdf:datatype="http://www.w3.org/2001/XMLSchema#string">pubmed</bp:db>
  </bp:PublicationXref>
  <bp:PathwayStep rdf:ID="PathwayStep_8be59743477e0ef5ef48278c592c3e8c">
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">REPLACED http://pathwaycommons.org/pc12/PathwayStep_8be59743477e0ef5ef48278c592c3e8c</bp:comment>
    <bp:stepProcess rdf:resource="http://identifiers.org/reactome/R-HSA-2173795"/>
  </bp:PathwayStep>
  <bp:Provenance rdf:ID="Provenance_2fd94e28e36931796fb9119fad6d4003">
    <bp:standardName rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Reactome</bp:standardName>
    <bp:displayName rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Reactome</bp:displayName>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">REPLACED http://pathwaycommons.org/pc12/Provenance_2fd94e28e36931796fb9119fad6d4003</bp:comment>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Source http://www.reactome.org/download/current/biopax.zip type: BIOPAX, Reactome v69 (only 'Homo_sapiens.owl') 28-May-2019</bp:comment>
  </bp:Provenance>
  <bp:UnificationXref rdf:ID="UnificationXref_reactome_R-HSA-2173793">
    <bp:idVersion rdf:datatype="http://www.w3.org/2001/XMLSchema#string">2</bp:idVersion>
    <bp:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Reactome stable identifier. Use this URL to connect to the web page of this instance in Reactome: http://www.reactome.org/cgi-bin/eventbrowser_st_id?ST_ID=R-HSA-2173793.2</bp:comment>
    <bp:id rdf:datatype="http://www.w3.org/2001/XMLSchema#string">R-HSA-2173793</bp:id>
    <bp:db rdf:datatype="http://www.w3.org/2001/XMLSchema#string">reactome</bp:db>
  </bp:UnificationXref>
  <bp:PublicationXref rdf:about="http://identifiers.org/pubmed/10531062">
    <bp:year rdf:datatype="http://www.w3.org/2001/XMLSchema#int">1999</bp:year>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Stroschein, SL</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Zhou, S</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Zhou, Q</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Luo, K</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Wang, Wei</bp:author>
    <bp:id rdf:datatype="http://www.w3.org/2001/XMLSchema#string">10531062</bp:id>
    <bp:source rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Science 286:771-4</bp:source>
    <bp:title rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Negative feedback regulation of TGF-beta signaling by the SnoN oncoprotein</bp:title>
    <bp:db rdf:datatype="http://www.w3.org/2001/XMLSchema#string">pubmed</bp:db>
  </bp:PublicationXref>
  <bp:UnificationXref rdf:ID="UnificationXref_molecular_interactions_ontology_MI_0359">
    <bp:id rdf:datatype="http://www.w3.org/2001/XMLSchema#string">MI:0359</bp:id>
    <bp:db rdf:datatype="http://www.w3.org/2001/XMLSchema#string">molecular interactions ontology</bp:db>
  </bp:UnificationXref>
  <bp:PublicationXref rdf:about="http://identifiers.org/pubmed/18568018">
    <bp:year rdf:datatype="http://www.w3.org/2001/XMLSchema#int">2008</bp:year>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Peerani, R</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Zandstra, PW</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Wrana, JL</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Sakuma, R</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Dembowy, J</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Yaffe, MB</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Rao, BM</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Samavarchi-Tehrani, P</bp:author>
    <bp:author rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Varelas, X</bp:author>
    <bp:id rdf:datatype="http://www.w3.org/2001/XMLSchema#string">18568018</bp:id>
    <bp:source rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Nat Cell Biol 10:837-48</bp:source>
    <bp:title rdf:datatype="http://www.w3.org/2001/XMLSchema#string">TAZ controls Smad nucleocytoplasmic shuttling and regulates human embryonic stem-cell self-renewal</bp:title>
    <bp:db rdf:datatype="http://www.w3.org/2001/XMLSchema#string">pubmed</bp:db>
  </bp:PublicationXref>
  <bp:BioSource rdf:about="http://identifiers.org/taxonomy/9606">
    <bp:xref rdf:resource="#UnificationXref_taxonomy_9606"/>
    <bp:displayName rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Homo sapiens</bp:displayName>
    <bp:name rdf:datatype="http://www.w3.org/2001/XMLSchema#string">BiologicalState8</bp:name>
    <bp:name rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Homo sapiens (human)</bp:name>
    <bp:name rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Human</bp:name>
    <bp:name rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Homo sapiens (Human)</bp:name>
    <bp:name rdf:datatype="http://www.w3.org/2001/XMLSchema#string">human</bp:name>
  </bp:BioSource>
  <bp:UnificationXref rdf:ID="UnificationXref_taxonomy_9606">
    <bp:id rdf:datatype="http://www.w3.org/2001/XMLSchema#string">9606</bp:id>
    <bp:db rdf:datatype="http://www.w3.org/2001/XMLSchema#string">taxonomy</bp:db>
  </bp:UnificationXref>
</rdf:RDF>
