Creator "igraph version 1.2.6 Mon May 24 16:15:53 2021"
Version 1
graph
[
  directed 1
  node
  [
    id 0
    name "AKT1"
  ]
  node
  [
    id 1
    name "ATM"
  ]
  node
  [
    id 2
    name "BTRC"
  ]
  node
  [
    id 3
    name "CTNNB1"
  ]
  node
  [
    id 4
    name "DVL1"
  ]
  node
  [
    id 5
    name "E2F1"
  ]
  node
  [
    id 6
    name "EP300"
  ]
  node
  [
    id 7
    name "ERBB4"
  ]
  node
  [
    id 8
    name "IKBKB"
  ]
  node
  [
    id 9
    name "LATS2"
  ]
  node
  [
    id 10
    name "NFKB1"
  ]
  node
  [
    id 11
    name "NFKBIA"
  ]
  node
  [
    id 12
    name "NRG1"
  ]
  node
  [
    id 13
    name "RELA"
  ]
  node
  [
    id 14
    name "SMAD2"
  ]
  node
  [
    id 15
    name "SRC"
  ]
  node
  [
    id 16
    name "WWTR1"
  ]
  node
  [
    id 17
    name "YAP1"
  ]
  node
  [
    id 18
    name "MEsaddlebrown"
  ]
  edge
  [
    source 0
    target 3
    weight 0.54267538461047
    capactiy 0.54267538461047
  ]
  edge
  [
    source 0
    target 6
    weight 0.522802171188717
    capactiy 0.522802171188717
  ]
  edge
  [
    source 0
    target 8
    weight 0.49630771197497
    capactiy 0.49630771197497
  ]
  edge
  [
    source 0
    target 15
    weight 0.839491795984053
    capactiy 0.839491795984053
  ]
  edge
  [
    source 0
    target 17
    weight 0.712771941204375
    capactiy 0.712771941204375
  ]
  edge
  [
    source 1
    target 5
    weight 0.0888772637673012
    capactiy 0.0888772637673012
  ]
  edge
  [
    source 2
    target 3
    weight 0.481800223451096
    capactiy 0.481800223451096
  ]
  edge
  [
    source 2
    target 10
    weight 0.696278053108198
    capactiy 0.696278053108198
  ]
  edge
  [
    source 2
    target 11
    weight 0.646750853305558
    capactiy 0.646750853305558
  ]
  edge
  [
    source 3
    target 6
    weight 0.757538410206268
    capactiy 0.757538410206268
  ]
  edge
  [
    source 3
    target 15
    weight 0.795126316322954
    capactiy 0.795126316322954
  ]
  edge
  [
    source 4
    target 3
    weight 0.579237133865558
    capactiy 0.579237133865558
  ]
  edge
  [
    source 5
    target 1
    weight 0.276459706775557
    capactiy 0.276459706775557
  ]
  edge
  [
    source 5
    target 10
    weight 0.667883657261182
    capactiy 0.667883657261182
  ]
  edge
  [
    source 5
    target 13
    weight 0.250193564213069
    capactiy 0.250193564213069
  ]
  edge
  [
    source 6
    target 3
    weight 0.569955967198013
    capactiy 0.569955967198013
  ]
  edge
  [
    source 6
    target 13
    weight 0.465997989244795
    capactiy 0.465997989244795
  ]
  edge
  [
    source 6
    target 14
    weight 0.474088857757527
    capactiy 0.474088857757527
  ]
  edge
  [
    source 7
    target 12
    weight 0.823777880587293
    capactiy 0.823777880587293
  ]
  edge
  [
    source 7
    target 17
    weight 0.70215876656991
    capactiy 0.70215876656991
  ]
  edge
  [
    source 8
    target 10
    weight 0.770840338144419
    capactiy 0.770840338144419
  ]
  edge
  [
    source 8
    target 11
    weight 0.578997076474558
    capactiy 0.578997076474558
  ]
  edge
  [
    source 8
    target 13
    weight 0.522754222241862
    capactiy 0.522754222241862
  ]
  edge
  [
    source 8
    target 15
    weight 0.718736089979504
    capactiy 0.718736089979504
  ]
  edge
  [
    source 9
    target 16
    weight 0.59285448458224
    capactiy 0.59285448458224
  ]
  edge
  [
    source 9
    target 17
    weight 0.544451740573551
    capactiy 0.544451740573551
  ]
  edge
  [
    source 10
    target 8
    weight 0.770840338144419
    capactiy 0.770840338144419
  ]
  edge
  [
    source 10
    target 11
    weight 0.866571103022669
    capactiy 0.866571103022669
  ]
  edge
  [
    source 10
    target 13
    weight 0.527707313600273
    capactiy 0.527707313600273
  ]
  edge
  [
    source 11
    target 2
    weight 0.646750853305558
    capactiy 0.646750853305558
  ]
  edge
  [
    source 11
    target 10
    weight 0.866571103022669
    capactiy 0.866571103022669
  ]
  edge
  [
    source 11
    target 13
    weight 0.628173886089191
    capactiy 0.628173886089191
  ]
  edge
  [
    source 12
    target 7
    weight 0.636195437579037
    capactiy 0.636195437579037
  ]
  edge
  [
    source 13
    target 10
    weight 0.715289756608529
    capactiy 0.715289756608529
  ]
  edge
  [
    source 13
    target 11
    weight 0.628173886089191
    capactiy 0.628173886089191
  ]
  edge
  [
    source 14
    target 6
    weight 0.661671300765783
    capactiy 0.661671300765783
  ]
  edge
  [
    source 15
    target 0
    weight 0.651909352975798
    capactiy 0.651909352975798
  ]
  edge
  [
    source 15
    target 3
    weight 0.607543873314699
    capactiy 0.607543873314699
  ]
  edge
  [
    source 15
    target 8
    weight 0.531153646971248
    capactiy 0.531153646971248
  ]
  edge
  [
    source 15
    target 11
    weight 0.507271527767375
    capactiy 0.507271527767375
  ]
  edge
  [
    source 16
    target 4
    weight 0.619774550496157
    capactiy 0.619774550496157
  ]
  edge
  [
    source 16
    target 14
    weight 0.4174483886449
    capactiy 0.4174483886449
  ]
  edge
  [
    source 18
    target 1
    weight 1
    capactiy 1
  ]
  edge
  [
    source 18
    target 4
    weight 1
    capactiy 1
  ]
  edge
  [
    source 18
    target 9
    weight 1
    capactiy 1
  ]
  edge
  [
    source 18
    target 12
    weight 1
    capactiy 1
  ]
]
