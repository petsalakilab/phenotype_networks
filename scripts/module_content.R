library(enrichR)
library(gprofiler2)
library(readr)
library(data.table)
library(dplyr)
source("~/cell_shapes/scripts/get_wgcna.R")
edgelist<-data.frame(read_delim("~/cell_shapes/sem_sim/semsimNet.txt", delim = "\t")) #get our graph. usually ./totalNetwork.txt 

wgcna.split<-get_wgcna(path_wgcna = "~/cell_shapes/genesprmodule/ALLgenesprmodule.tab", 
                       path_correlations = "~/cell_shapes/genesprmodule/correlations.txt", 
                       network = edgelist,
                       is.full = T)
correlations_table<-read.delim(file = "~/cell_shapes/genesprmodule/correlations.txt",header = FALSE) #../../genesprmodule/correlations.txt
correlations_table<-correlations_table[correlations_table$V1 != 1,]
correlations_table$V2<-as.numeric(correlations_table$V2)
correlations<-correlations_table$V1[correlations_table$V2 < -0.6 | correlations_table$V2 > 0.6]
wgcna.split.correlation<-wgcna.split[correlations] #subset modules that are only correlated with a shape Enrichr_Submissions_TF-Gene_Coocurrence

dbs <- listEnrichrDbs()$libraryName # ProteomicsDB_2020 #KEGG_2019_Human
dbs
down.up<-c(grep("down", dbs), grep("up", dbs), grep("Cell-lines", dbs), grep("Cell_Line", dbs),  grep("Tissues", dbs),  grep("TargetScan", dbs),grep("ARCHS4", dbs),
           grep("Enrichr_Submissions_TF-Gene_Coocurrence", dbs))
df.names<-data.frame()
for (module in rownames(strong.correlated.mods)) {
  enriched_prizes<- enrichr(wgcna.split[module][[1]], "Reactome_2015")
  enrichdt<-rbindlist(lapply(1:length(enriched_prizes), function(x){ setDT(enriched_prizes[[x]])[, id:=names(enriched_prizes)[x]]}), use.names=TRUE, fill=TRUE)
  #combined score has to be above the 90% quantile 
  enrichdt<-enrichdt[enrichdt$Adjusted.P.value < 0.05,]
  enrichdt<-enrichdt[order(enrichdt$Adjusted.P.value),]
  prio.order<-c(which(enrichdt$id %in% c("KEGG_2016", "Reactome_2016", "Panther_2016", "WikiPathways_2016", "MSigDB Hallmark 2020")), 
            which(!enrichdt$id %in% c("KEGG_2016", "Reactome_2016", "Panther_2016", "WikiPathways_2016", "MSigDB Hallmark 2020")))
  top<-enrichdt[prio.order,]$Term[1:20]
  top_db<-enrichdt[prio.order,]$id[1:20]
  Overlap<-enrichdt[prio.order,]$Overlap[1:20]
  P.value<-enrichdt[prio.order,]$P.value[1:20]
  Adjusted.P.value<-enrichdt[prio.order,]$Adjusted.P.value[1:20]
  Odds.Ratio<-enrichdt[prio.order,]$Odds.Ratio[1:20]
  Genes<-enrichdt[prio.order,]$Genes[1:20]
  
  
  row.to.add <- data.frame(module, top,top_db, Overlap, P.value, Adjusted.P.value,Odds.Ratio, Genes)
  df.names<-rbind(df.names, row.to.add)
  
}
split.names <- df.names %>%
  group_by(module)
#write_csv(split.names, file = "~/cell_shapes/data/full_module_names.csv")
#split.names<-group_split(split.names)

for (module in names(wgcna.split.correlation)) {
  message(module)
}

#enriched_prizes[[1]]$Term[enriched_prizes[[1]]$Adjusted.P.value < 0.1]

for (protein in wgcna.split["MElightslateblue"][[1]]) {
  message(protein)
}
test<-results_all[results_all$cluster != "all" & results_all$padj < 0.01,]