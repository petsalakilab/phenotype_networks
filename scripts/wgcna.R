#!/usr/bin/Rscript

# AUTHOR:	Eirini Petsalaki
# Takes gene expression data (from init_data.R) and uses the WGCNA R package to construct a weighted gene correlated network. 
# Then, using our paired phenotypic data, we correlate the wgcn derived gene expression modules (GEMs) with these features, and 
# extract the modules that are relevant and write them in a file. 
# 
# INPUT: 
#          GeneXData,             	Gene expression data (counts pr gene) derived from init_data.R  
#          shapefeatmedian.csv    	Median features pairs with cell lines (or the common groups that 
#				  	can be paired with the expression data ) 
#
# OUTPUT:   
#	   correlation.txt                txt. file describing the relationships between GEMs and phenotype
#				                         	  features (Only those with |PCC|<0.5 and P<0.05).
#	   ALLgenesprmodule.tab		        file with the names of the GEMs and their gene contents. 
#

library(readr)
library(WGCNA)
library(flashClust)

path<-"~/phenotype_networks/data"
setwd(path)

####Data PREP####

GeneXData <- read.csv("../data/expression/GeneXData.csv")
#GeneXData$CAMA1<-NULL
GeneXData <- GeneXData[,1:17]
gene.IDs <- GeneXData$GeneID
gene.names <- GeneXData$GeneName
# format properly for WGCNA
rownames(GeneXData) <- gene.IDs
GeneXData$GeneName <- NULL
GeneXData$GeneID <- NULL
GeneXData$X <- NULL
GeneXDiff<-GeneXData
GeneXData <- as.data.frame(t(GeneXData))
dim(GeneXData)
# 14 cell lines, 15304 genes
# LOG2 Transformation
logXdat <- log2(GeneXData+1)
colnames(logXdat) <- gene.IDs

#stuff for diff X
#GeneXDiff <- log2(GeneXDiff+1) #INCLUDING THIS SEEMS TO FUCK WIH MY BCV. ASK GIANNIS. 
GeneXDiff$GeneID <- gene.IDs
GeneXDiff$GeneName <- gene.names
#write.csv(GeneXDiff, file = "../data/GeneXDataNorm.csv")

# Run this to check if there are gene outliers
gsg = goodSamplesGenes(logXdat, verbose = 3)
gsg$allOK

if (!gsg$allOK)
  
{if (sum(!gsg$goodGenes)>0)
  printFlush(paste("Removing genes:", paste(names(logXdat)[!gsg$goodGenes], collapse= ", ")));
  if (sum(!gsg$goodSamples)>0)
    printFlush(paste("Removing samples:", paste(rownames(logXdat)[!gsg$goodSamples], collapse=", ")))
  logXdat= logXdat[gsg$goodSamples, gsg$goodGenes]
}
# Shape features table (traits)
shapefeat <- read.csv("./phenotype_features/shapefeatmedian.csv")
row.names(shapefeat) <- shapefeat$X
shapefeat$X <- NULL
shapefeat <- as.data.frame(shapefeat)
table(rownames(shapefeat)==rownames(GeneXData))

####RUN WGCA####

#Construct a gene co-expression matrix and generate modules
#build a adjacency "correlation" matrix

enableWGCNAThreads()

pickSoftThreshold(logXdat)
#pick Soft power threshold based on the results of the preceding function.
softPower = 9
adjacency = adjacency(logXdat, power = softPower, type = "signed") #specify network type
dim(adjacency)

# Construct Networks
#translate the adjacency into topological overlap matrix and calculate the corresponding dissimilarity:
TOM = TOMsimilarity(adjacency, TOMType="signed") # specify network type
dissTOM = 1-TOM

# Generate Modules --------------------------------------------------------


# Generate a clustered gene tree
geneTree = flashClust(as.dist(dissTOM), method="average")
plot(geneTree, xlab="", sub="", main= "Gene Clustering on TOM-based dissimilarity", labels= FALSE, hang=0.04)
#This sets the minimum number of genes to cluster into a module
minModuleSize = 30
dynamicMods = cutreeDynamic(dendro= geneTree, distM= dissTOM, deepSplit=2, pamRespectsDendro= FALSE, minClusterSize = minModuleSize)
dynamicColors= labels2colors(dynamicMods)
MEList= moduleEigengenes(logXdat, colors= dynamicColors,softPower = 9)
MEs= MEList$eigengenes
MEDiss= 1-cor(MEs)
METree= flashClust(as.dist(MEDiss), method= "average")


#plots tree showing how the eigengenes cluster together
#png(file="clusterwithoutmodulecolors10.png")
#plot(METree, main= "Clustering of module eigengenes", xlab= "", sub= "",cex = 0.7)
#set a threhold for merging modules. In this example we are not merging so MEDissThres=0.0
#MEDissThres = 0.4
MEDissThres = 0.0
merge = mergeCloseModules(logXdat, dynamicColors, cutHeight= MEDissThres, verbose =3)
mergedColors = merge$colors
mergedMEs = merge$newMEs

#plot dendrogram with module colors below it
#png(file = "cluster10.png")
plotDendroAndColors(geneTree, cbind(dynamicColors, mergedColors), c("Dynamic Tree Cut", "Merged dynamic"), dendroLabels= FALSE, hang=0.03, addGuide= TRUE, guideHang=0.05)
plotDendroAndColors(geneTree, dynamicColors, "Dynamic Tree Cut", dendroLabels= FALSE, hang=0.03, addGuide= TRUE, guideHang=0.05)
moduleColors = mergedColors
colorOrder = c("grey", standardColors(50))
moduleLabels = match(moduleColors, colorOrder)-1
MEs = mergedMEs

####Correlate Phenotype Traits####

#Define number of genes and samples
nGenes = ncol(logXdat)
nSamples = nrow(logXdat)
#Recalculate MEs with color labels
MEs0 = moduleEigengenes(logXdat, moduleColors)$eigengenes
MEs = orderMEs(MEs0)
plotEigengeneNetworks(MEs[seq(dim(MEs)[1],1),], "Eigengene dendrogram", marDendro = c(0,4,2,0),plotHeatmaps = FALSE)
moduleTraitCor = cor(MEs, shapefeat, use= "p")
moduleTraitPvalue = corPvalueStudent(moduleTraitCor, nSamples)


#Print correlation heatmap between modules and traits
#textMatrix= paste(signif(moduleTraitCor, 2), "\n(",
#                  signif(moduleTraitPvalue, 1), ")", sep= ""
strong.correlated.mods<-moduleTraitCor[as.logical(rowSums((moduleTraitCor > 0.5  | moduleTraitCor < -0.5) & moduleTraitPvalue < 0.05) != 0),]
#replace all the nonsignificant associatons with 0 so it is easie to analyse 
strong.correlated.mods[!(strong.correlated.mods > 0.5  | strong.correlated.mods < -0.5)] <- 0 
textMatrix= paste(signif(strong.correlated.mods, 2))
dim(strong.correlated.mods)== dim(strong.correlated.mods)
#write_csv(x = data.frame(strong.correlated.mods, 
#			modules = rownames(strong.correlated.mods)), 
#			file = "~/phenotype_networks/data/wgcna/strong.correlated.mods.csv")
colnames(shapefeat) <- c("Cell Area","Cell Width To Length", "Centers Distance",   "Ruffliness",
                         "Cellular Protrusion Area",    "Nucleus by Cell Area",      "Nucleus Roundness",
                         "Nucleus Area",        "Nucleaus Width To Length",  "Neighbour Fraction"   )
#display the corelation values with a heatmap plot
mod_names<-read.csv(file = "../data/module_names.csv")
labeledHeatmap(Matrix= strong.correlated.mods,
               xLabels= colnames(shapefeat),
               yLabels= mod_names[match(rownames(strong.correlated.mods), mod_names$ME_names),]$new_name,
               ySymbols= mod_names[match(rownames(strong.correlated.mods), mod_names$ME_names),]$new_name,
               colorLabels= T,
               colors= blueWhiteRed(50),
               textMatrix= textMatrix,
               setStdMargins= F,
               cex.text= 0.6,
               cex.lab.y = .8,
               cex.lab.x = 1,
               zlim= c(-1,1),
               main= paste("Module-trait relationships"))
textMatrix= signif(moduleTraitCor, 2)
par(mar= c(7, 8.5, 6, 6))
labeledHeatmap(Matrix= t(moduleTraitCor),
               xLabels= names(MEs),
               yLabels= colnames(shapefeat),
               ySymbols= colnames(shapefeat),
               colorLabels= FALSE,
               colors= blueWhiteRed(50),
               setStdMargins= FALSE,
               cex.text= 0.6,
               cex.lab.y = 0.8,
               cex.lab.x = 1,
               zlim= c(-1,1),
               main= paste("Module-trait relationships"))

genes <- colnames(logXdat)
gene_mod <- cbind(genes, module=moduleColors)


###################################################################################3
# AFTER CLUSTER: UNCOMMENT THE NEXT LINES
# GET EVERYTHING NECESSARY FOR GENE SET ENRICHMENT
for (k in (1:length(moduleColors))) {
  moduleColors[k] <- paste("ME",moduleColors[k],sep="")
}
gene_mod <- cbind(genes, module=moduleColors)
gene_mod <- as.data.frame(gene_mod)

modules <- unique(moduleColors)
modulek <- modules[1]
genesprmod <- list(gene_mod[gene_mod$module == modulek,"genes"])
for (k in (2:length(modules))) {
  modulek <- modules[k]
  genesprmod <- append(genesprmod,list(gene_mod[gene_mod$module == modulek,"genes"]))
}
names(genesprmod)<-modules
write.table(genesprmod[modulek],file = "./modules/ALLgenesprmodule.tab", row.names = F, append = F,sep="\t",quote = F)
for (i in names(genesprmod)[!names(genesprmod) %in% modulek]) {
  write.table(genesprmod[i],file = "./modules/ALLgenesprmodule.tab", row.names = F, append = T,sep="\t",quote = F)
}


# EXTRACT Modules that correlate with each attribute with an absolute value >= 0.5
# Then, keep those with a p-val < 0.05
moduleTraitCor <- as.data.frame(moduleTraitCor)
attributes <- colnames(moduleTraitCor)
shapefeat<-list()
for (i in (1:length(attributes))) {
  nam = paste(colnames(moduleTraitCor)[i])
  mods <- moduleTraitCor[abs(as.numeric(moduleTraitCor[,i])) >= 0.5,]
  modnames <- row.names(mods)
  mods <- moduleTraitCor[abs(as.numeric(moduleTraitCor[,i])) >= 0.5,nam]
  names(mods)<-modnames
  for (m in modnames) {
    if (as.numeric(moduleTraitPvalue[m,i]) > 0.05){
      mods <- mods[-(which(names(mods) == m))]
    }
  }
  shapefeat[[attributes[i]]]<-mods 
}
first_ <- names(shapefeat)[1]
write.table(first_, file = "./modules/correlations.txt",row.names = T,col.names = F,append = F, sep = "\t", quote = F)
write.table(shapefeat[first_], file = "./modules/correlations.txt",row.names = T,col.names = F,append = T,sep = "\t", quote = F)
#shapefeat is a list of lists
for (feature in names(shapefeat)[!names(shapefeat) %in% first_]) {
  write.table(feature, file = "./modules/correlations.txt",row.names = T,col.names = F,append = T, sep = "\t", quote = F)
  write.table(shapefeat[feature], file = "./modules/correlations.txt",row.names = T,col.names = F,append = T,sep = "\t", quote = F)
}

