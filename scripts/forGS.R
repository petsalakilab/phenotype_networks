######################################## OPEN forGS.RData
## WRITE FILES WITH MODULES AND GENES
setwd("~/phenotype_networks/data/modules")

readRDS("~/phenotype_networks/data/wgcna/forGS.RData")
for (i in names(CellArea)) {
  write.table(genesprmod[i],file = "CellArea.tab", row.names = F, append = T,sep="\t")
}
for (i in names(Cell_WidthToLength)) {
  write.table(genesprmod[i],file = "Cell_WidthToLength.tab", row.names = F, append = T,sep="\t")
}
for (i in names(centers_distance)) {
  write.table(genesprmod[i],file = "centers_distance.tab", row.names = F, append = T,sep="\t")
}
for (i in names(NF)) {
  write.table(genesprmod[i],file = "NF.tab", row.names = F, append = T,sep="\t")
}
for (i in names(Nuc_WidthToLength)) {
  write.table(genesprmod[i],file = "Nuc_WidthToLength.tab", row.names = F, append = T,sep="\t")
}
for (i in names(NucbyCytoArea)) {
  write.table(genesprmod[i],file = "NucbyCytoArea.tab", row.names = F, append = T,sep="\t")
}
for (i in names(Nuclear_Roundness)) {
  write.table(genesprmod[i],file = "Nuclear_Roundness.tab", row.names = F, append = T,sep="\t")
}
for (i in names(NucleusArea)) {
  write.table(genesprmod[i],file = "NucleusArea.tab", row.names = F, append = T,sep="\t")
}
for (i in names(Protrusion_area)) {
  write.table(genesprmod[i],file = "Protrusion_area.tab", row.names = F, append = T,sep="\t")
}
for (i in names(ruffliness)) {
  write.table(genesprmod[i],file = "ruffliness.tab", row.names = F, append = T,sep="\t")
}

shapefeat = list(Cell_WidthToLength,CellArea,centers_distance,NF,Nuc_WidthToLength,
              NucbyCytoArea,Nuclear_Roundness,ruffliness)
shapenames = c("Cell_WidthToLength","CellArea","centers_distance","NF","Nuc_WidthToLength",
              "NucbyCytoArea","Nuclear_Roundness","ruffliness")
fun1 <- function (some_variable, name=deparse(substitute(some_variable))) {
  name
}

for (s in (1:length(shapefeat))) {
  write.table(shapenames[s], file = "correlations.txt",row.names = T,col.names = F,append = T, sep = "\t")
  write.table(shapefeat[s], file = "correlations.txt",row.names = T,col.names = F,append = T,sep = "\t")
}

for (i in names(genesprmod)) {
  write.table(genesprmod[i],file = "ALLgenesprmodule.tab", row.names = F, append = T,sep="\t")
}

