#!/usr/bin/Rscript

# AUTHOR: Charlie Barker using function by Ioannis Kamzolas 
#
# Uses library DOROTHEA to find the enrichments of regulons from our differential expression data. 
#
# INPUT: 
#          DEGS.txt,                file summarsing differentially expressed genes identified by deSEQ.R
#          human_network.csv        TF human regulons taken from https://saezlab.github.io/dorothea/
#
# OUTPUT:   
#	 
#   TF.csv (out_path)               file describing significantly differntially regulated TFs. 

library(dorothea)
library(org.Hs.eg.db)
library(readr)
#path of pathways to write
path<-"~/phenotype_networks/data/" 
#path of pathway names to read
setwd(path)
res <- "~/phenotype_networks/data/differential_exp/hetero_DEGs.txt"
out_path<-"~/phenotype_networks/data/DOROTHEA/hetero.csv"

#code kindly sent by Giannis Kamzolas 

#  ---TF Activities---
# Inputs:
# 1) -Mouse regulon file (already created mouse network - we don't need to create it again and it's the same for the analysis of all the models)
#    -Human regulon file ------------//---------------------------//---------------------------//---------------------------//------------------
# 2) D.E. Signature (file from DESeq2 (with gene names in the last column))


library(viper)
#install.packages("purrr")
library("purrr")
#install.packages("dplyr")
library("dplyr")



df2regulon = function(df) {
  regulon = df %>%
    split(.$tf) %>%
    map(function(dat) {
      tf = dat %>% distinct(tf) %>% pull()
      targets = setNames(dat$mor, dat$target)
      likelihood = dat$likelihood
      list(tfmode =targets, likelihood = likelihood)
    })
  return(regulon)
}



my_TFA <- function(mypath, organism, outfile) #mypath is deseq filename
{
  if(organism == "HUMAN")
    Regulon_file<- read.csv("DOROTHEA/human_network.csv", header=T) ###Open the human regulon file
  
  ###subset to the threshold - keep only the most confident TFs
  Regulon_file<- Regulon_file[Regulon_file$confidence=='A'| Regulon_file$confidence=='B'| Regulon_file$confidence=='C' | Regulon_file$confidence=='D',]
  DEsignature <- read.table(file = mypath, sep = ",", header = TRUE) #Read the DEGs file
  ens2symbol <- AnnotationDbi::select(org.Hs.eg.db, #add gene names 
                                      key=DEsignature$X, 
                                      columns="SYMBOL",
                                      keytype="ENSEMBL")
  #get symbol 
  ens2symbol <- as_tibble(ens2symbol)
  DEsignature <- inner_join(DEsignature, ens2symbol, by=c("X"="ENSEMBL"))
  # Exclude probes with unknown or duplicated gene symbol
  DEsignature<-DEsignature[!(is.na(DEsignature$padj) | DEsignature$padj==""), ]
  # Estimatez-score values for the GES. Check VIPER manual for details
  myStatistics = matrix(DEsignature$log2FoldChange, dimnames = list(DEsignature$SYMBOL, 'log2FC') )
  myPvalue = matrix(DEsignature$padj, dimnames = list(DEsignature$SYMBOL, 'adj.P.Val') )
  mySignature = (qnorm(myPvalue/2, lower.tail = FALSE) * sign(myStatistics))[, 1]
  mySignature = mySignature[order(mySignature, decreasing = T)]
  # Estimate TF activities
  mrs = msviper(ges = mySignature, regulon = df2regulon(Regulon_file), ges.filter = F, minsize = 4)
  
  
  TF_activities = data.frame(Regulon = names(mrs$es$nes),
                             Size = mrs$es$size[ names(mrs$es$nes) ], 
                             NES = mrs$es$nes, 
                             p.value = mrs$es$p.value, 
                             FDR = p.adjust(mrs$es$p.value, method = 'fdr'))
  TF_activities = TF_activities[ order(TF_activities$p.value), ]
  # Save results
  if (organism == "HUMAN")
   write.csv(TF_activities, file = outfile)
}




#############################
#############################
#Viper - Transcription Factor Activities using the DEGs obtained by DESeq2
#############################
#############################

#Human_EPOS
my_TFA(res, "HUMAN",out_path)

#plot
dorothea<-read_csv(out_path, col_names = TRUE)
#sig.fgsea<-suppressWarnings(separate(data = sig.fgsea, col = pathway, into = "pathway",sep = "%"))
sig.dorothea<-dorothea[(dorothea$NES > 1 | dorothea$NES < -1) & dorothea$FDR <0.05,]

sig.dorothea<-sig.dorothea[order(sig.dorothea$NES,decreasing = TRUE),]
luminal<-sig.dorothea
print(head(luminal))

library(ggplot2)
ggplot(sig.dorothea, aes(reorder(Regulon, NES), NES)) +
  geom_col(aes(fill=FDR<0.05)) +
  coord_flip() +
  labs(x="Regulon", y="Normalized Enrichment Score",
       title="Regulons") + 
  theme_minimal()


dotters<-rbind(data.frame(basal, type = "basal"),data.frame(luminal, type ="luminal"))
dotters$lfc <- -log2(dotters$FDR)
dotters$sig <- dotters$FDR < 0.05
#plot
dots<-ggplot(data = dotters, aes(x=reorder(X1, -lfc), y = reorder(type, lfc), color = NES, size = lfc, shape = sig)) + 
  geom_point()  + scale_shape_manual(values = c(19, 0)) 
dots + scale_color_continuous(type = "viridis") + theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1)) + theme(text = element_text(size = 20))  

ggplot(data = dotters, aes(x=NES, y = X1, color = type, size = lfc)) + 
  geom_point() + theme_classic()

