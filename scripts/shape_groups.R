# script that visuales the cell lines variance in the given phenotype in question: clustering based on k-means algorithm
# 
# AUTHOR:	C.Barker
# INPUT: 
#          shapefeatmedian.csv,                 Cell shape data median values for phenotypes per cell line.
#          pk_subtypes.csv,                     file containing literature based subtypes of cell lines. 
#          shapefeat.csv                        raw phenotype data.
#
# OUTPUT:  
#          
#          shape_clusters.csv                   csv containing the cell lines, clustered according to their cell shapes 


library(ggbiplot)
library(BBmisc)
library(tidyverse)  # data manipulation
library(cluster)    # clustering algorithms
library(factoextra)
path<-"~/phenotype_networks/data/phenotype_features"
setwd(path)
shape<-read.csv("shapefeatmedian.csv",row.names = "X")
genetic.cluster <- read.csv("pk_subtypes.csv",header = TRUE,stringsAsFactors = FALSE)
genetic.cluster[genetic.cluster$Cell.Line == "ZR75.1",1] <- "ZR751"
rownames(genetic.cluster) <- genetic.cluster$Cell.Line
genetic.cluster<-genetic.cluster[row.names(shape),]



shape_norm<-BBmisc::normalize(shape,range = c(0,1))

plot(shape_norm)
#sanity check, cor(normalised) = cor(normalised) data 
pca.shape<-prcomp(shape_norm)
ggbiplot(pca.shape, labels = row.names(shape),var.axes = FALSE,groups = genetic.cluster$Authors.shape.cluster)
#variance poorely explained by first two principle components :()
#3D plot doesnt really make much difference, and still fails to serperate different subtypes. 

##No obvious clusters, the different subtypes dont match up, which means to seperate on shape we need a more cell-shape based divisions.
#find
distance <- get_dist(shape,method = "euclidean")
fviz_dist(distance, order = T, gradient = list(low = "#00AFBB", mid = "white", high = "#FC4E07"))
tw<-c()
for (i in c(1:12)) {
  tw <- c(tw, kmeans(shape, centers = i, nstart = 200)$tot.withinss)
}
elbow<-data.frame(tw, c(1:12))
ggplot(data=elbow, aes(x=c.1.12., y=tw)) +
  geom_line()+
  geom_point() +
  geom_point(data=elbow[elbow$c.1.12. == 3, ],
             pch=21, fill=NA, size=6, colour="red", stroke=1) +
  theme_bw() +
  xlab("No. of clusters") + ylab("Total within-cluster variation") + theme(text = element_text(size=20))
#write_csv(x = elbow, 
#          file = "~/cell_shapes/manuscript/figures/cell shape figures/supplementary/figure_csv/S2A.csv")

k2 <- kmeans(shape, centers = 3, nstart = 200)
str(k2)

fviz_cluster(k2, data = shape)
#looks BETTER.
#original paper had their own clusters.. why not use them too? 
clusters<-data.frame(k2$cluster)
clusters$Cell.Line<-row.names(clusters)
#uncomment if you want to right
#write.csv(as.data.frame(clusters),file = "./shape_clusters.csv")

#get boxplots of all the different attributes 
shape<-read.csv("shapefeat.csv")
shape[shape$Cell.line == "MDA-MB-157",]$Cell.line <- "MDAMB157"
shape[shape$Cell.line == "ZR75.1",]$Cell.line <- "ZR751"
shape[shape$Cell.line == "MDA-MB-231",]$Cell.line <- "MDAMB231"

clusters_df<-data.frame(k2$cluster)
clusters_df$Cell.Lines<-row.names(clusters_df)
shape_clusters<-merge(x = clusters_df, y = shape, by.x = "Cell.Lines", by.y = "Cell.line")
colnames(shape_clusters)<-c("Cell", "Cluster", "Cell.Area", "Cell_WidthtoLength", "Centers.Distance", "Rufflines", "Protrusion.Area",
                            "NucbyCytoArea", "Nuclear.Roundness", "Nucleus.Area", "Nuc_widthtoLength", "NF")
shape_clusters$Cluster<-as.character(shape_clusters$Cluster)
Cluster<-"Cluster"
# Libraries
library(hrbrthemes)
library(viridis)
for (cell.variable in colnames(shape_clusters)) {
  print(cell.variable)
  #violin
  print(  shape_clusters %>%
            ggplot( aes_string(x=Cluster, y=cell.variable, fill=Cluster)) +
            geom_violin() +
            scale_fill_viridis(discrete = TRUE, alpha=0.5, option="A") +
            theme_ipsum() +
            theme(
              legend.position="none",
              plot.title = element_text(size=11)
            ) +
            ggtitle(cell.variable) +
            xlab(""))

}
# Compute the analysis of variance
# "Cell"   "Cluster"  "Cell.Area" "Cell_WidthtoLength" "Centers.Distance"  "Rufflines"  "Protrusion.Area" "NucbyCytoArea" "Nuclear.Roundness"  "Nucleus.Area" "Nuc_widthtoLength"  "NF"      
res.aov <- aov(NF ~ Cluster, data = shape_clusters)
# Summary of the analysis
TukeyHSD(res.aov)

#plot pca
#pca
ggbiplot(pca.shape, var.axes = FALSE) + geom_point(aes(shape=genetic.cluster$Genetic.cluster, 
                                                       colour=as.character(k2$cluster)), 
                                                   size=4) +
  geom_text(aes(label = genetic.cluster$Cell.Line), position = position_nudge(y = -0.1)) 

