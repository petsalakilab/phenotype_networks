#!/usr/bin/python3

#!/usr/bin/python3


  # Takes ENS id modules (in format of list with > before module name (see ALLgenesprmodule.tab) and converts to SPROT id) 
  # 
  # AUTHOR:   Eirini Petsalaki
  # USAGE:    python convtosprot.py genesprmodule.tab out_file.tab
  # OUTPUT:   list of modules and the gene names contained within them. 




import sys

mypath = sys.argv[0].split("/")
if len(mypath) > 1:
    mart_export = open("{}/gsid-to-sprot.txt".format("/".join(mypath[:-1]),"r"))
else:
    mart_export = open("gsid-to-sprot.txt","r")
sprotid = {}

line = mart_export.readline()
line = mart_export.readline()
while line!= "":
    line = line.strip()
    row = line.split("\t")
    if len(row)>1:
        sprotid[row[0]] = row[1]
    line = mart_export.readline()
mart_export.close()

infile = open(sys.argv[1],"r")
outfile = open(sys.argv[2],"w")
for line in infile:
    if line.startswith("ENSG"):
        if line.strip() in sprotid:
            outfile.write(sprotid[line.strip()]+"\n")
    else:
        outfile.write(">"+line)

infile.close()
outfile.close()

