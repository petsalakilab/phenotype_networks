# Integrates the reactome pathways from reactome_tf_reg, the TRRUST and DOROTHEA TFs in one continuous network via PCSF. 
# 
# AUTHOR:	C.Barker
# INPUT: 
#          TRRUST_Fisher_results,          Directory containing file p/module with TRRUST fisher results 
#          strong.correlated.mods.csv,     csv of table showing which modules are significantly correlated with a shape 
#          TFAs                            logical - do you want the full list of wgna modules
#          biopax_shape_pathway.sif
# OUTPUT:   
#          cs_net.txt                      cell shape regulatory network 
#          plots                           plots to make sure that the PCSF solution isnt ridiculous (adding many spurious terms - not scale free)

library(OmnipathR)
library(PCSF)
library(readr)
library(igraph)
library(BBmisc)
library(dplyr)
library(tidyr)

set.seed(Sys.time())
seed<-sample(1:20000, 1, replace=F)
print(seed) #1446 #19607 #18362 for legacy with dorothea below .05
set.seed(18362)

construct_interactome <- #taken directly from the package PCSF except now allowing for directed networks
  function(ppi){
    
    # Checking function arguments
    if (missing(ppi))
      stop("  Need to specify a list of edges to construct an interaction network.
    Provide a data.frame composed of three columns, where each row corresponds 
    to an edge in which the first element is a head node, the second element 
    is a tail node, and the last element represents the cost of the edge.")
    if (nrow(ppi)<1 || ncol(ppi) != 3 || class(ppi) != "data.frame")
      stop("  Need to provide a data.frame composed of three columns, where each row corresponds 
    to an edge in which the first element is a head node, the second element 
    is a tail node, and the last element represents the cost of the edge.")
    
    # Interpolate the node prizes
    node_names = unique(c(as.character(ppi[,1]),as.character(ppi[,2])))
    
    # Contruct an interaction network as igraph object
    ppi.graph = graph.data.frame(ppi[,1:2],vertices=node_names,directed=T)
    E(ppi.graph)$weight=as.numeric(ppi[,3])
    ppi.graph = simplify(ppi.graph)
    
    return (ppi.graph)
    
  }

path<-"~/phenotype_networks/data/"
setwd(path)

####prep interactome####

#pull interactome off omnipath

#interactions <- import_omnipath_interactions(datasets = c("omnipath")) #import from OMNIPATH
#interactome<-data.frame(interactions$source_genesymbol, 
#                        interactions$target_genesymbol, 
#                        1/interactions$n_resources) #set the cost value to be the inverse of the number of sources.

#orrr we pull our local version. 

interactome<-read.csv("~/phenotype_networks/data/omnipath.csv") #or get local omnipath - so it doesnt change

colnames(interactome)<-c("protein1",  "protein2", "cost")
ppi<-construct_interactome(interactome) #function adapted from the PCSF R package 
ppi_list<-igraph::as_data_frame(ppi)

#####extract the tfs correlated with the various shape modules as the prizes 


cell.shape.path<-c("~/phenotype_networks/trrust/TRRUST_Fisher_results")

cellShapeTFs_2<-c()

modules<-list.files(path=cell.shape.path, pattern="*.txt", full.names=TRUE, recursive=FALSE)
modulenames<-unlist(lapply(strsplit(modules, "/"), tail, n = 1L))
modulenames<-substr(modulenames,1,nchar(modulenames)-11)
correlations_table<-read_csv(file = "~/phenotype_networks/data/wgcna/strong.correlated.mods.csv") #../../genesprmodule/correlations.txt
modules<-modules[modulenames %in% correlations_table$modules]
for (module in modules) {
  tf.file<-suppressMessages(read_delim(module, delim = "\t", col_names = FALSE))
  cellShapeTFs_2<-c(cellShapeTFs_2, tf.file[tf.file$X3 < 0.1,]$X1) #only take those with adjusted pvalue under 0.1
}  
cellShapeTFs_2<-strsplit(cellShapeTFs_2, "_")
cellShapeTFs_2<-unique(unlist(cellShapeTFs_2))
######get dorothea analysis 
tfa.path<-c("~/phenotype_networks/data/DOROTHEA/")
tfa.files<- c("basal.csv",   "luminal.csv", "hetero.csv")
dorothea.tfs<-c()
for (file in tfa.files) {
  tfa.df<-suppressMessages(read_delim(paste(tfa.path, file, sep = ""), delim = ",", col_names = TRUE))
  dorothea.tfs <- c(dorothea.tfs, tfa.df$Regulon[tfa.df$FDR < 0.1])
}
dorothea.tfs<-unique(dorothea.tfs)

intersect(dorothea.tfs, cellShapeTFs_2) #PAX6 and ETV6 are common between the differential sequence anlysis and the WGCNA 

####reactome pathways found by eirini####

sif <- suppressMessages(read_delim("~/phenotype_networks/data/legacy_biopax_shape_pathway.sif",delim = "\t"))
reactome<-intersect(unique(c(sif$PARTICIPANT_A, sif$PARTICIPANT_B)), unique(c(interactome$protein1, interactome$protein2)))
prizes<-union(c(reactome, dorothea.tfs), cellShapeTFs_2) #merge all of our interesting genes 
prize_file<-data.frame(prizes, 100) #if you can think of a better way to allocate prizes id like to hear it 
colnames(prize_file)<-c("name", "prize")

####prep data for pcsf#####
terminal<-as.double(prize_file$prize)
names(terminal)<-prize_file$name
####parameters ####
n<-30 #no. of runs30
r<-5 #adding random noise to edge costs  5
w<-40 #number of trees in output 40
b<-1 #tuning node prizes 1
mu<-0.005 #hub penalisation #0.005

######perfor pcsf n times adding some noise to produce more robust network. #####
subnet <- PCSF_rand(ppi,
                    terminal,
                    n = n,
                    r = r,
                    w = w,
                    b = b,
                    mu = mu) 

#problem: PCSF output is no longer directed - so we need to recover that information from the input before we can
#use the results in downstream analysis 

#extract sub network as edgelist  
edgelist<-igraph::as_data_frame(subnet)
nodes<-unique(c(edgelist$from, edgelist$to)) #nodes in the network 

#so pcsf doesnt removes any direction information and so we need this for loop to go through. 
directed_subnetwork<-data.frame(from = character(0), to = numeric(0), weight = numeric(0))
for (i in c(1:nrow(edgelist))) {
  #take edges in ppi that are in our subwork
  newline<-ppi_list[ppi_list$from == edgelist[i,]$from & ppi_list$to == edgelist[i,]$to | ppi_list$to == edgelist[i,]$from & ppi_list$from == edgelist[i,]$to,]
  directed_subnetwork<-rbind(directed_subnetwork , newline)
}
#the result of the recovered network will be slightly bigger than the output, as we get both forward and backward edges in our recovered network 
#if they exist, whereas we dont in our original pcsf subnetwork. maybe this is a source of our false positives. 


#####add edges indicating correlation between tfs and wgcna modules 
cell.directoris<-c("CellArea",  "Cell_WidthToLength", 
                   "centers_distance",  "NF",  
                   "NucbyCytoArea",  "Nuclear_Roundness",  
                   "Nuc_WidthToLength",  "ruffliness")
tfa.to.module<-data.frame()
for (cell in cell.directoris) {
  path<-paste(cell.shape.path, cell,sep = "/")
  path<-cell.shape.path
  modules<-list.files(path=path, pattern="*.txt", full.names=TRUE, recursive=FALSE)
  for (module in modules) {
    module.splt<-strsplit(module, "/")[[1]]
    module.name<-strsplit(module.splt[length(module.splt)], "_")[[1]][1]
    tf.file<-suppressMessages(read_delim(module, delim = "\t", col_names = FALSE))
    if(nrow(tf.file) != 0){
      tf.df<-data.frame(tf.file, module.name)
      tfa.to.module<-rbind(tfa.to.module, tf.df)
    }
  }  
}
tfa.to.module<-data.frame(tfa.to.module$X1, tfa.to.module$X3, tfa.to.module$module.name)
tfa.to.module<-unique(tfa.to.module)
colnames(tfa.to.module)<-c("tf", "pvalue", "module")
#normalise p values to resemble weights. 
tfa.to.module <- tfa.to.module[tfa.to.module$pvalue < 0.1,] #remove those edges with low pvalues 
tfa.to.module$weight <- -log2(tfa.to.module$pvalue) #transform p values into minus log space
tfa.to.module$weight <- BBmisc::normalize(method = "range",
                                          as.numeric(tfa.to.module$weight),
                                          range = c(0.00001,1)) #normalise between a range of 0.1 and 1 to get weight
tfa.to.module<-tfa.to.module[tfa.to.module$module %in% correlations_table$modules,]
#add sign (is it necessary?) IMPORTANT 
tfa.to.module<- tfa.to.module %>% separate(tf, 
                                           c("TF", "sign"))
#####add edges indicating correlation between wgcna modules and cell shape parameters 
shape.correlation.path<-"/home/charlie/cell_shapes/trrust/tfFORmodules/fisher"
shape.correlation<-suppressMessages(read_delim("/home/charlie/cell_shapes/trrust/tfFORmodules/fisher/correlations.txt",
                                               delim = "\t",
                                               col_names = FALSE))
shape.vector<-c()
for (i in seq(1, nrow(shape.correlation))) {
  if (shape.correlation[i,1] == 1) {
    x<-as.character(shape.correlation[i,2])
  }
  shape.vector<-c(shape.vector, x)
}
colnames(shape.correlation)<-c("module", "weight")
shape.correlation<-data.frame(shape.correlation, shape.vector)
shape.correlation<-shape.correlation[!is.na(suppressWarnings(as.numeric(shape.correlation$weight))),]

#add TF to module layer and module to phenotype layer 

tf.module.layer<-tfa.to.module[tfa.to.module$TF %in% unique(c(directed_subnetwork$from, directed_subnetwork$to)),] #get tfs that are in our network, and extract edges to wgcna modules
module.phenotype.layer<-shape.correlation[shape.correlation$module %in% unique(tf.module.layer$module),] #get wgcna modules from that and extract edges to shape phenotypes 

####write file for signal flow (all layers)#####

#include sign 
signed.weights<-c()
for (i in 1:nrow(tf.module.layer)) {
  if(tf.module.layer[i,2] == "Repression"){
    signed.weights<-c(signed.weights, tf.module.layer[i,]$weight*-1)
  }
  else {signed.weights<-c(signed.weights, tf.module.layer[i,]$weight)}
}
tf.module.layer<-data.frame(tf.module.layer, signed.weights)
tf.module.layer <- data.frame(tf.module.layer$TF, tf.module.layer$module, tf.module.layer$signed.weights)
module.phenotype.layer <- data.frame(module.phenotype.layer$module, module.phenotype.layer$shape.vector, as.numeric(module.phenotype.layer$weight))
cnames<-c("from", "to", "weight")
colnames(tf.module.layer)<-cnames
colnames(module.phenotype.layer)<-cnames

#these nodes are ones that will be tested in the kinase peturbation analysis
#they are significant because they are one edge upstream from our transcriptions 
#factors predicted to have an effect on cell shape.
#In future we want to look at inhibition of kinases progressively further away from our TF and phenotype layers. 
kinases.to.peturb<-directed_subnetwork[directed_subnetwork$to %in% tf.module.layer$from,]$from

#normalise weights of signaling network and add the sign 
directed_subnetwork$weight <- BBmisc::normalize(method = "range",
                                                as.numeric(directed_subnetwork$weight),
                                                range = c(0.01,1)) #normalise between a range of 0.1 and 1 to get weight

#turn to weight NOT cost 
non_signed<-directed_subnetwork
directed_subnetwork$weight <- 1-directed_subnetwork$weight

signs<-data.frame(interactions$source_genesymbol, interactions$target_genesymbol, interactions$consensus_stimulation)
colnames(signs)<-c("from", "to", "sign")
signs$merge<- paste(signs$from, signs$to, sep = "_")
signs$from<-NULL
signs$to<-NULL
directed_subnetwork$merge<- paste(directed_subnetwork$from, directed_subnetwork$to, sep = "_")
signed.directed_subnetwork<-merge(directed_subnetwork, signs, by.x="merge", by.y="merge")
signed.directed_subnetwork$merge<-NULL
for (i in 1:nrow(signed.directed_subnetwork)) {
  if(signed.directed_subnetwork[i,4] == 0){
    signed.directed_subnetwork$weight[i]<-signed.directed_subnetwork$weight[i]*-1
  }
}
signed.directed_subnetwork$sign<-NULL

#####write file for heat diffusion  (just signaling layer) 
#these also have to be reversed because for PCSF they are costs, and they need to be weights for heat diffusion. 
#write_delim(non_signed,path = "./test.txt", delim = "\t") #write as tab delim edge file  

total.network<-rbind(signed.directed_subnetwork, tf.module.layer, module.phenotype.layer) #merge all the layers into one sif 
all(sort(unique(module.phenotype.layer$from)) == sort(unique(tf.module.layer$to)))

#WRITE GRAPH

#write_delim(total.network,file = "~/phenotype_networks/network_results/test.txt", delim = "\t") #write as tab delim edge file  

#write 
used.net<-read.delim("~/phenotype_networks/network_results/test.txt")
used.nodes <- unique(c(used.net$from, used.net$to))
used.net$weight <- sqrt((used.net$weight)^2)
used.net$weight[used.net$weight == 0] <- 0.00001
used.subnet <- graph.data.frame(used.net, directed = TRUE)

library(enrichR)
dbs <- listEnrichrDbs()

#maybe do a venn diagram here to really study what terms we have lost and what we have gained
library(ggwordcloud)
b.centrality<-data.frame(sort(betweenness(used.subnet), decreasing = TRUE))
b.centrality$NODE <- rownames(b.centrality)
b.centrality$terminal <- as.numeric(!b.centrality$NODE %in% names(terminal))
colnames(b.centrality)<-c("betweeness", "NODE", "term")
set.seed(42)
ggplot(b.centrality[1:60,], aes(label = NODE, size = betweeness, color = term)) +
  geom_text_wordcloud() +
  scale_size_area(max_size = 20) +
  theme_minimal()
length(used.nodes)
#we cam see from this that our genes of interest seem to dominate the more central nodes - maybe express this as a distribution for supplemnary 
dbs <- "Panther_2016" # ProteomicsDB_2020 #KEGG_2019_Human
enriched_prizes_terminal<- enrichr(b.centrality$NODE[b.centrality$term == 0], dbs)
list_terms_terminal<-enriched_prizes_terminal[[1]]$Term[enriched_prizes_terminal[[1]]$Adjusted.P.value < 0.001]

enriched_prizes_pcsf<- enrichr(b.centrality$NODE[b.centrality$term == 1], dbs)
list_terms_pcsf<-enriched_prizes_pcsf[[1]]$Term[enriched_prizes_pcsf[[1]]$Adjusted.P.value < 0.001]
intersect(list_terms_pcsf, list_terms_terminal)
to.plot<-rbind(data.frame(enriched_prizes_pcsf[[1]], source = "PCSF"),data.frame(enriched_prizes_terminal[[1]], source = "SEED"))
to.plot$lfc.adj.p <- -log10(to.plot$Adjusted.P.value)
to.plot$Term <- substr(to.plot$Term,1,nchar(to.plot$Term)-20)
ggplot(data = to.plot[to.plot$Adjusted.P.value < 0.001,], aes(x=Combined.Score, y = reorder(Term, Combined.Score), color = source, size = lfc.adj.p)) + 
  geom_point() + theme_classic()



